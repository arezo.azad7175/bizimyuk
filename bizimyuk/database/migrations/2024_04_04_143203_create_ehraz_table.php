<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ehraz', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('reg_phone');
            $table->bigInteger('melicode');
            $table->bigInteger('hoshmand');
            $table->bigInteger('hoshmandv');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ehraz');
    }
};
