<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mainbar', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('rahgiri');
            $table->integer('value_mohasebe');
            $table->text('datebar');
            $table->text('source');
            $table->text('destinition');
            $table->text('ellam');
            $table->text('time_start');
            $table->text('time_end');
            $table->integer('te');
            $table->text('packagekala');
            $table->integer('id_mohasebe');
            $table->integer('id_karayeh');
            $table->integer('id_pardakht');
            $table->integer('price');
            $table->bigInteger('reg_phone');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mainbar');
    }
};
