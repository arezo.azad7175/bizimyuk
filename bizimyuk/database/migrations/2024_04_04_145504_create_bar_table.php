<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bar', function (Blueprint $table) {
            $table->id();
            $table->integer('mainbar_index');
            $table->integer('id_vasile');
            $table->integer('id_tkh');
            $table->integer('id_load');
            $table->integer('id_oback');
            $table->text('ddate');
            $table->time('ttime');
            $table->bigInteger('reg_phone');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bar');
    }
};
