<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('havale', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('rahgiri');
            $table->integer('nobat')->nullable;
            $table->text('ddate');
            $table->text('dd');
            $table->time('ttime');
            $table->text('source');
            $table->text('target');
            $table->text('company');
            $table->text('packagekala');
            $table->integer('status');
            $table->integer('gharardad');
            $table->integer('tonaz');
            $table->text('barname');
            $table->integer('value_ts');
            $table->bigInteger('phone');
            $table->text('name');
            $table->text('pelak');
            $table->integer('id_archive');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('havale');
    }
};
