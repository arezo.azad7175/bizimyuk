<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('profile', function (Blueprint $table) {
            $table->id();
            $table->integer('id_mojaves');
            $table->integer('allow');
            $table->bigInteger('reg_phone');
            $table->text('name');
            $table->text('family');
            $table->integer('status');
            $table->integer('olaviyat');
            $table->text('loading_place');
            $table->integer('gharardad');
            $table->integer('amount');
            $table->text('address');
            $table->bigInteger('phone');
            $table->text('namemodir');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('profile');
    }
};
