<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('navgan', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('reg_phone');
            $table->integer('id_vasile');
            $table->integer('id_load');
            $table->integer('id_oback');
            $table->integer('id_tkh');
            $table->text('p');
            $table->integer('p1');
            $table->text('p2');
            $table->integer('p3');
            $table->integer('p4');
            $table->text('type_nav');
            $table->text('brand_name');
            $table->integer('create_year');
            $table->text('dates');
            $table->integer('ds');
            $table->text('dateb');
            $table->integer('db');
            $table->text('datebs');
            $table->integer('dbs');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('navgan');
    }
};
