<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('b_archive', function (Blueprint $table) {
            $table->id();
            $table->integer('ds');
            $table->integer('status');
            $table->integer('price');
            $table->integer('closing');
            $table->integer('reset');
            $table->integer('te');
            $table->time('timecontrol');
            $table->integer('nobat')->nullable();
            $table->time('time_step_2');
            $table->text('date_step_2');
            $table->bigInteger('rahgiri');
            $table->bigInteger('main_rahgiri');
            $table->integer('id_bar');
            $table->bigInteger('reg_phone');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('b_archive');
    }
};
