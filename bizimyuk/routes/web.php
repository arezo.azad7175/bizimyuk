<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\PagesController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Salon\CarryController;
use App\Http\Controllers\Layout\AboutUsController;
use App\Http\Controllers\Salon\TurnningController;
use App\Http\Controllers\Auth\ForgetPassController;
use App\Http\Controllers\Layout\ChangePassController;
use App\Http\Controllers\Auth\SendAgainCodeController;
use App\Http\Controllers\Companies\CompaniesController;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\Drivers\DriversEditController;
use App\Http\Controllers\Drivers\DriversShowController;
use App\Http\Controllers\Salon\WaittingQueueController;
use App\Http\Controllers\Drivers\DriverSendSmsController;
use App\Http\Controllers\Drivers\DriversFilterController;
use App\Http\Controllers\Drivers\DriversUpdateDataController;
use App\Http\Controllers\Reports\FinancialReport\Drivers\WeekReportController;
use App\Http\Controllers\Reports\FinancialReport\Drivers\MonthReportController;
use App\Http\Controllers\Reports\FinancialReport\Drivers\RangeReportController;
use App\Http\Controllers\Reports\FinancialReport\Companies\WeekCompaniesReportController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// show-Auth
Route::get('/' ,[PagesController::class , 'showLogin'])->name('login');
Route::get('/forgetPass' ,[PagesController::class , 'showForgetPass'])->name('forget.Pass');
Route::get('/get-forgetPass' ,[PagesController::class , 'showResiveForgetPass'])->name('resive.forgetPass');
Route::get('/send-loginCode' ,[PagesController::class , 'showSendingLoginCode'])->name('send.loginCode');

// Auth-login
Route::post('/login' , [LoginController::class , 'login'])->name('login.check');
Route::post('/login/code' , [LoginController::class , 'checkCode'])->name('login.checkCode');

// Auth-forgetpass
Route::post('/forgetPass' , [ForgetPassController::class , 'checkUser'])->name('forget.checkUser');
Route::post('/forgetPass/code/{id}' , [ForgetPassController::class , 'checkCode'])->name('forget.checkCode');

// Auth-logout
Route::post('/logout' , [LogoutController::class , 'logout'])->name('logout');

// send-again-code
Route::post('/sendCode' , [SendAgainCodeController::class , 'sendCode'])->name('sendCode');

// first authentication then show 
Route::group(['middleware' => 'auth'], function() {

    // layaout-master
    Route::post('/changePass' , [ChangePassController::class , 'changePass'])->name('changePass');
    Route::get('/about-us',[AboutUsController::class , 'aboutUs'])->name('aboutUs');

    // dashboard
    Route::get('/dashboard' , [DashboardController::class , 'showDashboard'])->name('dashboard');

    // salon/turnning
    Route::get('/salon/turnning' , [TurnningController::class , 'showTurnning'])->name('turnning');
    Route::post('/salon/turnning/confirm/{order_id}' , [TurnningController::class , 'confirmTurnning'])->name('confirm.turnning');
    Route::post('/salon/turnning/cancel/{order_id}' , [TurnningController::class , 'cancelTurnning'])->name('cancel.turnning');

    // salon/carry
    Route::get('/salon/carry' , [CarryController::class , 'showCarry'])->name('carry');

    // salon/waittigQueue
    Route::get('/salon/waittingQueue' , [WaittingQueueController::class , 'showWaittingQueue'])->name('waittingQueue');

    // drivers
    Route::get('/drivers' , [DriversShowController::class , 'showDrivers'])->name('drivers');
    Route::get('/drivers/{status}' , [DriversFilterController::class , 'filterDrivers'])->name('filter-driver');
    // edit-driver
    Route::get('/drivers/edit/{id}' , [DriversEditController::class , 'showEditDriver'])->name('show-edit-driver');
    Route::post('/drivers/edit' , [DriversEditController::class , 'editDriver'])->name('edit-driver');
    Route::post('/drivers/sendSms' , [DriverSendSmsController::class , 'driverSendSms']);

    // companies
    Route::get('/companies' , [CompaniesController::class , 'showCompanies'])->name('companies');

    // report_drivers
    Route::get('/financialReport/Week' , [WeekReportController::class , 'dayOfWeekReport'])->name('financialWeekReport');
    Route::get('/financialReport/Month' , [MonthReportController::class , 'weekOfMonthReport'])->name('financialMonthReport');
    Route::post('/financialReport/Range' , [RangeReportController::class , 'rangeOfDateReport'])->name('financialRangeReport');

    // report_compaies
    Route::get('/financialReport/companies/Week' , [WeekCompaniesReportController::class , 'weekCompaniesReport'])->name('financialCompaniesWeekReport');
});

