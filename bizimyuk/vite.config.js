import { defineConfig } from 'vite';

import laravel from 'laravel-vite-plugin';



export default defineConfig({

    plugins: [

        laravel({

            input: [

                // CSS
                // template-Auth-css
                'resources/css/Auth/template/main.css',
                'resources/css/Auth/template/util.css',
                'resources/css/Auth/template/flex-slider.css',
                'resources/css/Auth/template/font-awesome.css',
                'resources/css/Auth/template/templatemo-softy-pinko.css',
                'resources/css/Auth/template/animate/animate.css',
                'resources/css/Auth/template/bootstrap/bootstrap.min.css',
                'resources/css/Auth/template/hamburgers/hamburgers.min.css',
                'resources/css/Auth/template/select/select.min.css',

                // template-css
                'resources/css/template/bootstrap.min.css',
                'resources/css/template/flex-slider.css',
                'resources/css/template/font-awesome.css',
                'resources/css/template/templatemo-softy-pinko.css',

                // css
                'resources/css/drivers.css',
                'resources/css/reports.css',
                'resources/css/salon.css',



                // JS
                // template_Auth_js
                'resources/js/Auth/template/main.js',
                'resources/js/Auth/template/bootstrap/bootstrap.min.js',
                'resources/js/Auth/template/bootstrap/popper.js',
                'resources/js/Auth/template/bootstrap/tooltip.js',
                'resources/js/Auth/template/jquery/jquery-3.2.1.min.js',
                'resources/js/Auth/template/select/select.min.js',
                'resources/js/Auth/template/tilt/tilt.jquery.min.js',

                // Auth
                'resources/js/Auth/timer.js',

                // persion-datePicker
                'resources/js/persion-datePicker/persian-date.js',
                'resources/js/persion-datePicker/persian-datepicker.js',

                // Driver-js
                'resources/js/Drivers/driverPicZoom.js',
                'resources/js/Drivers/driverSearchAutomaticly.js',
                'resources/js/Drivers/driverSendSms.js',

                // reports-js
                'resources/js/Reports/FinancialReport/report_driver.js',

                // salon-js
                'resources/js/Salon/salonTurnningModal.js',

                // template-js
                'resources/js/template/custom.js',
                'resources/js/template/imgfix.min.js',
                'resources/js/template/jquery-2.1.0.min.js',
                'resources/js/template/popper.js',
                'resources/js/template/scrollreveal.min.js',
                'resources/js/template/waypoints.min.js',

                ],

            refresh: true,

        }),

    ],

});

