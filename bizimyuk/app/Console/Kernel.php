<?php



namespace App\Console;



use Illuminate\Support\Facades\Artisan;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;



class Kernel extends ConsoleKernel

{

    /**

     * Define the application's command schedule.

     */

    protected function schedule(Schedule $schedule): void

    {

        $schedule->command('queue:work --tries=3 --delay=2 --stop-when-empty')->withoutOverlapping()->runInBackground();
        // Artisan::call('queue:work');

    }



    /**

     * Register the commands for the application.

     */

    protected function commands(): void

    {

        $this->load(__DIR__.'/Commands');



        require base_path('routes/console.php');

    }

}

