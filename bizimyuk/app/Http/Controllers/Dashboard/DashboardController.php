<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hekmatinasser\Verta\Verta;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    // dashboard_page
    public function showDashboard()
    {
        // در ازائه ی تستی نیاز به اعمال تاریخ نیست در صورت نیاز از حالت کامنت خارج کنید
        // 1402/03/12
        // $date_Now =  Verta::now()->formatJalaliDate();
        // 14020312
        // $dateNow =  Verta::now()->format('Ymd');

        // در انتظار نوبت 
        $turnning = DB::table('havale')
        ->where('status' , 100)
        // ->Where('ddate' , $date_Now)
        ->count();

        // پایان عملیات
        $finish = DB::table('havale')
        ->where('status' , 4)
        // ->where('dd' , $dateNow )
        ->count();

        // ذر انتظار بارگیری
        $loading = DB::table('havale')
        ->where('status' , 2)
        // ->where('dd' , $dateNow )
        ->count();

        //در انتظار تخلیه
        $carry = DB::table('havale')
        ->where('status' , 3)
        // ->where('dd' , $dateNow )
        ->count();

        // لغو بار 
        $cancel = DB::table('havale')
        ->where('status' , 5)
        // ->where('dd' , $dateNow )
        ->count();

        return view('Dahboard.dashboard' , ['turnning' => $turnning , 'finish'=>$finish , 'loading' => $loading , 'carry' => $carry , 'cancel' => $cancel]);
    }
}
