<?php

namespace App\Http\Controllers\Layout;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ChangePassController extends Controller
{
    public function changePass(Request $request)
    {
        // check old password in DB and Input
        if(Auth::user()->password == $request->pass)
        {
            // check two Inputs new pass and confirm that
            if($request->newPass == $request->confirmNewPass)
            {
                // change password in DB
                User::where('id' , Auth::user()->id)->update(['password' => $request->newPass]);
                return redirect()->back()->with('changeSuccess' , 'رمز  عبور با موفقیت تغییر کرد');
            }
            return redirect()->back()->with('changeError' , ' تکرار رمز عبور جدید مشابه رمز عبور وارد شده نمی باشد');
        }
        return redirect()->back()->with('changeError' ,'رمز عبور وارد شده مشابه رمز عبور ذخیره شده نمی باشد');
    }
}
