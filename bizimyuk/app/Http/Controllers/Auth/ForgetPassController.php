<?php



namespace App\Http\Controllers\Auth;



use App\Models\User;
use App\Helper\Helper;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;


class ForgetPassController extends Controller
{

    // check-user
    public function checkUser(Request $request)
    {
        $userExist= User::where('mobile',$request->mobile)->doesntExist();
        $check_Captcha = Helper::check_captcha();

        if(!$userExist)
        {
            if($check_Captcha)
            {
                // refresh sessions
                $request->session()->regenerate();

                // user
                $user = User::where('mobile',$request->mobile)->first();

                // send-varifacation-code
                $mobile = $user->mobile;
                $code = rand(100000 , 999999);
                Helper::sendSms($mobile , $code);

                // save-code
                Cache::put($user->id , Hash::make($code));

                return redirect()->route('resive.forgetPass')->with('user_id' , $user->id);
            }
            return redirect()->back()->with('forgetError' , 'کد وارد شده نادرست است');
        }
        return redirect()->back()->with('forgetError' , 'کاربری با این شماره ومجود ندارد');
    }


    // check_sending_code
    public function checkCode($id , Request $request)
    {
        // user
        $user = User::whereId($id)->first();

        // getcode
        $code = Cache::get($user->id);

        // check-true
        if(Hash::check($request->pass , $code ))
        {
            // create_pass
            $newpass = Str::random(3).rand(100 , 999);
            // get_mobile
            $mobile = $user->mobile;
            // set_pass_DB
            User::whereId($id)->update(['password' => $newpass]);
            // send_username_password_in_sms
            Helper::sendInfoSms($mobile , $user->username , $newpass);
            // login_user
            Auth::login($user);

            return redirect()->route('dashboard')->with('user_id' , $user->id);
        }

        // check-false
        return redirect()->back()->with('forgetError' , 'کد وارد شده نادرست است')->with('user_id' , $id);
    }
}

