<?php



namespace App\Http\Controllers\Auth;
use App\Models\User;
use App\Helper\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;

class SendAgainCodeController extends Controller
{
    public function sendCode()
    {
        $id = Cache::get('userId');
        $user = User::whereId($id)->first();

        // send-varifacation-code
        $mobile = $user->mobile;
        $code = rand(100000 , 999999);
        $verifyCode = Helper::sendSms($mobile , $code);

        // save-code
        Cache::put($user->id , Hash::make($code));

        return redirect()->back();
    }
}

