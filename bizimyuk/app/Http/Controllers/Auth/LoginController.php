<?php



namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Helper\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;

class LoginController extends Controller
{
    // check-user
    public function login(Request $request)
    {
        $userExist = User::where('username',$request->username)->doesntExist();
        if(!$userExist)
        {
            if(Helper::check_captcha())
            {
                // user
                $user = User::where('username',$request->username)->first();

                // check-user-true
                if($user->username === $request->username && $user->password ===$request->password)
                {
                    // refresh sessions
                    $request->session()->regenerate();

                    // send-verifaction-code
                    $mobile = $user->mobile;
                    $code = rand(100000 , 999999);
                    Helper::sendSms($mobile , $code);

                    // save-code
                    Cache::put($user->id , Hash::make($code));
                    Cache::put('userId' , $user->id);
                    return redirect()->route('send.loginCode');
                }
                // check-user-false
                return redirect()->back()->with('loginError' , 'نام کاربری یا رمزعبور اشتباه است');
            }
            return redirect()->back()->with('loginError' , 'کد وارد شده نادرست است');
        }
        return redirect()->back()->with('loginError' , 'کاربری با این مشخصات وجود ندارد+6' );
    }


    // check-sending-code
    public function checkCode( Request $request)
    {
        $id = Cache::get('userId');
        // user
        $user = User::whereId($id)->first();

        // getcode
        $code = Cache::get($user->id);

        // check-true
        if(Hash::check($request->pass , $code ))
        {
            Auth::loginUsingId($user->id , true);
            return redirect()->route('dashboard');
        }

        // check-false
        return redirect()->back()->with('loginError' , ' کد وارد شده نادرست است');
    }









}

