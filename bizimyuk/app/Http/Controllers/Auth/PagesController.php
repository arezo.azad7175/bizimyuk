<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    // login_page
    public function showLogin() 
    {
        return view('Auth.login');
    }

    // forget_pass_page
    public function showForgetPass()
    {
        return view('Auth.forgetPass');
    }

    // recive_code_forget_pass_page
    public function showResiveForgetPass()
    {
        return view('Auth.resiveForgetPass');
    }

    // recive_code_login_page
    public function showSendingLoginCode()
    {
        return view('Auth.sendLoginCode');
    }

}
