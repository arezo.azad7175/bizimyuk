<?php

namespace App\Http\Controllers\Drivers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DriversEditController extends Controller
{
    // show edit page for driver
    public function showEditDriver($id)
    {
        $driver = DB::table('profile')
            ->where('profile.id' , $id)
            ->join('ehraz' , 'ehraz.reg_phone' ,'=' , 'profile.reg_phone')
            ->join('navgan' , 'profile.reg_phone' ,'=','navgan.reg_phone')
            ->first();

        $type_load = DB::table('typeload')
            ->get();

        $type_oback = DB::table('typeoback')
            ->get();

        $type_car = DB::table('typecar')
            ->get();
        
        $type_room = DB::table('typetkh')
            ->select('id' , 'name')
            ->get();

        $loading_place = DB::table('loading_place')
            ->select('place' , 'description as name')
            ->get();

        return view('Drivers.driversEdit' , 
            [   'driver'=>$driver , 
                'type_load'=>$type_load , 
                'type_oback'=>$type_oback , 
                'type_car'=>$type_car,
                'loading_place' => $loading_place,
                'type_room' =>$type_room,
            ]);
    }

    
    // edit driver in DB
    public function editDriver(Request $request)
    {
        // for change persion date to english
        $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

        DB::table('profile')->where('phone' , $request->phone)->update([
            'name' => $request->name,
            'family' => $request->lastName,
            'status' => $request->status,
            'olaviyat'=> $request->olaviat,
            'loading_place'=> $request->loading_place,
            'gharardad'=> $request->agreement,
        ]);

        DB::table('ehraz')->where('reg_phone' , $request->phone)->update([
            'melicode'=> $request->nationalCode,
            'hoshmand'=> $request->smartCardDriver,
            'hoshmandv'=> $request->smartCardNavgan, 
        ]);

        DB::table('navgan')->where('reg_phone' , $request->phone)->update([
            'id_vasile'=> $request->vasile,
            'id_load'=> $request->unloading_type,
            'id_oback'=> $request->Back_room_door,
            'id_tkh' => $request->type_room,
            'p1'=> $request->p1,
            'p3'=> $request->p3,
            'p4'=> $request->p4,
            'type_nav' => $request->type_nav,
            'brand_name'=> $request->brand_name,
            'create_year' => $request->create_year,
            'dates'=> str_replace($persian, $english,$request->bime_saleth),
            'ds'=> str_replace($persian, $english, str_replace("/", "",$request->bime_health)),
            'dateb'=> str_replace($persian, $english,$request->fani),
            'db'=> str_replace($persian, $english,str_replace("/", "",$request->fani)),
            'datebs'=> str_replace($persian, $english,$request->bime_health),
            'dbs'=> str_replace($persian, $english,str_replace("/", "",$request->bime_health)),
        ]);

        return redirect()->back()->with(
            [
                'success_update' => 'اطلاعات شما با موفقیت به روزرسانی شد' ,
                'reason_status' => $request->reason,
            ]);
    }
}
