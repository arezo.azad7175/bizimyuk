<?php

namespace App\Http\Controllers\Drivers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Hekmatinasser\Verta\Facades\Verta;

class DriversShowController extends Controller
{
    public function showDrivers()
    {
        // get all records
        $drivers = DB::table('profile')
        ->join('loading_place' , 'loading_place.place' ,'=' , 'profile.loading_place')
        ->join('navgan' , 'profile.reg_phone' , '=' , 'navgan.reg_phone')
        ->where('profile.id_mojaves' , 0)
        ->selectRaw("profile.*,loading_place.description , navgan.p ,(SELECT COUNT(*) FROM `archive` WHERE `archive`.`reg_phone` = `profile`.`reg_phone`) As archive_count")
        ->get();

        return view('Drivers.drivers' , ['drivers' => $drivers]);
    }
}
              