<?php

namespace App\Http\Controllers\Drivers;

use App\Helper\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;

class DriverSendSmsController extends Controller
{
    public function driverSendSms(Request $request)
    {
        if($request->status == 0)
        {
            Helper::deactiveDriver($request->phone, $request->fullName, $request->reason);
        }
        else if($request->status != 0)
        {
            Helper::activeDriver($request->phone , $request->fullName);
        }

        return response()->json(['msg'=>"success"]);
    }
}
