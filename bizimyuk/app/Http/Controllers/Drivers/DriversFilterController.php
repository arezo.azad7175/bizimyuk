<?php

namespace App\Http\Controllers\Drivers;

use Illuminate\Http\Request;
use Hekmatinasser\Verta\Verta;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DriversFilterController extends Controller
{
    public function filterDrivers($status)
    {
        // not active records
        if($status == '0')
        {
            $drivers = DB::table('profile')
            ->join('loading_place' , 'loading_place.place' ,'=' , 'profile.loading_place')
            ->join('navgan' , 'profile.reg_phone' , '=' , 'navgan.reg_phone')
            ->where('profile.id_mojaves' , 0)
            ->where('profile.status' , 0 )
            ->selectRaw("profile.*,loading_place.description , navgan.p ,(SELECT COUNT(*) FROM `archive` WHERE `archive`.`reg_phone` = `profile`.`reg_phone`) As archive_count")
            ->get();
        }

        // active records
        elseif($status == '1')
        {
            $drivers = DB::table('profile')
            ->join('loading_place' , 'loading_place.place' ,'=' , 'profile.loading_place')
            ->join('navgan' , 'profile.reg_phone' , '=' , 'navgan.reg_phone')
            ->where('profile.id_mojaves' , 0)
            ->where('profile.status' , 1 )
            ->selectRaw("profile.*,loading_place.description , navgan.p ,(SELECT COUNT(*) FROM `archive` WHERE `archive`.`reg_phone` = `profile`.`reg_phone`) As archive_count")
            ->get();
        }

        // all records
        elseif($status == 'all')
        {
            $drivers = DB::table('profile')
            ->join('loading_place' , 'loading_place.place' ,'=' , 'profile.loading_place')
            ->join('navgan' , 'profile.reg_phone' , '=' , 'navgan.reg_phone')
            ->where('profile.id_mojaves' , 0)
            ->selectRaw("profile.*,loading_place.description , navgan.p ,(SELECT COUNT(*) FROM `archive` WHERE `archive`.`reg_phone` = `profile`.`reg_phone`) As archive_count")
            ->get();
        }

        return view('Drivers.drivers' , ['drivers' => $drivers]);
    }
}
