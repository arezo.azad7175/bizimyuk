<?php

namespace App\Http\Controllers\Reports\FinancialReport\Drivers;

use Illuminate\Http\Request;
use Hekmatinasser\Verta\Verta;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class WeekReportController extends Controller
{
    public function dayOfWeekReport()
    {
        // get first of week date
        $FirstOfWeek= Verta::now()->startWeek();
        // get tomorrow date becuse in diff we need today in count and must day finish 00:00
        $Tomorrow =  Verta::tomorrow();
        // get count of day in start and end date
        $interval = $FirstOfWeek->diffDays($Tomorrow);
        // get tomorrow & first of week with db format
        $TomorrowFormated = $Tomorrow->format('Ymd');
        $FirstOfWeekFormated=  $FirstOfWeek->format('Ymd');
        // days of week
        $period = ['شنبه' ,'یکشنبه' ,'دوشنبه' ,'سه شنبه' , 'چهارشنبه' , 'پنجشنبه' ,'جمعه'];

        // sum of price in dates , day by day 
        $date_daily = $FirstOfWeekFormated;
        for ($i=1; $i <= $interval; $i++) 
        { 
            $amounts[] = DB::table('archive')
                ->whereBetween('status' , [2,4])
                ->where('ds' ,  $date_daily )
                ->sum('price');  
            $date_daily  =  $date_daily +1;
        }

        // if we are in middlle or first of week 
        $count = 7-$interval;
        for ($i=1; $i <= $count; $i++) { 
            $amounts[] = 0;
        }

        // overal info for now week
        $date_week = $TomorrowFormated;
        for ($i=1; $i <= $interval; $i++)
        {
            $date_week  =  $date_week -1;
            $overalWeekNow[] = DB::table('archive')
                ->whereBetween('status' , [2,4])
                ->where('ds' ,  $date_week)
                ->sum('price');    
        }

        // overal info for last week
        $FirstOfLastWeek  =  Verta::now()->subWeek()->startWeek()->format('Ymd'); 
        for ($i=1; $i <= 7; $i++)
        {
            $overalWeekLast[] = DB::table('archive')
                ->whereBetween('status' , [2,4])
                ->where('ds' ,  $FirstOfLastWeek )
                ->sum('price');
            $FirstOfLastWeek  =  $FirstOfLastWeek +1;
        }

        // get finish carry in week
        $date_Carry = $FirstOfWeekFormated;
       for ($i=1; $i <= $interval ; $i++) { 
            $carryInWeek[] = DB::table('havale')
            ->where('status' , 4)
            ->where('dd' , $date_Carry)
            ->count();
            $date_Carry = $date_Carry+1;  
       }
    
        // send data to view   
        return view('Reports.FinancialReport.driversReport' , 
        [
            'amounts'=>$amounts , 
            'amountsLast'=>$overalWeekLast,
            'period'=>$period , 
            'carryInWeek'=>array_sum($carryInWeek),
            'overalWeekNow'=> array_sum($overalWeekNow), 
            'overalWeekLast'=> array_sum($overalWeekLast),
        ]);
    }
}
