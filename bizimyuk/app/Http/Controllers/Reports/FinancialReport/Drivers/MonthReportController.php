<?php

namespace App\Http\Controllers\Reports\FinancialReport\Drivers;

use Illuminate\Http\Request;
use Hekmatinasser\Verta\Verta;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MonthReportController extends Controller
{
    public function weekOfMonthReport()
    {
        // get first of month date
        $FirstOfMonth= Verta::now()->startMonth();
        // get tomorrow date becuse in diff we need today in count and must day finish 00:00
        $Tomorrow =  Verta::tomorrow();
        // get count of day in start month and end date
        $interval = $FirstOfMonth->diffDays($Tomorrow);
        // first of month & tomorrow formated
        $FirstOfMonthFormated=$FirstOfMonth->format('Ymd');
        $TomorrowFormated = $Tomorrow->format('Ymd');
        // we need days of week for get count days of week
        $week_days=[
            'Saturday'=>0,
            'Sunday'=>1,
            'Monday'=>2,
            'Tuesday'=>3,
            'Wednesday'=>4,
            'Thursday'=>5,
            'Friday'=>6,
        ];
        // get and send weeks
        $period=['هفته 1','هفته 2','هفته 3','هفته 4','هفته 5'];


        // sum of price of current month , week by week 
        // get first of current month
        $firstOfCurrentMonth = $FirstOfMonth;
        $firstOfCurrentMonthFormated= $FirstOfMonthFormated;
        // get end of current month
        $endOfCurrentMonth =  Verta::now()->endMonth();
        // finih action when month ended
        if($firstOfCurrentMonth != $endOfCurrentMonth)
        {
            for ($i=0; $i <= 5 ; $i++) { 
                // get count of days in week
                $weekDays = 7 -($week_days[$firstOfCurrentMonth->toCarbon()->dayName]);
                for ($j=0; $j < $weekDays ; $j++) 
                { 
                    $amount_day[]= DB::table('archive')
                        ->whereBetween('status' , [2,4])
                        ->where('ds' ,  $firstOfCurrentMonthFormated )
                        ->sum('price');  
                    // add one day
                    $firstOfCurrentMonth->addDay(1); 
                    $firstOfCurrentMonthFormated = $firstOfCurrentMonthFormated + 1;     
                }
                // sum amounts of one by one weeks
                $amounts[] = array_sum($amount_day); 
                $amount_day= null;
            }
        }

        // sum of price of last month , week by week 
        // get first of last month
        $FirstOfLastMonth  = Verta::now()->subMonth()->startMonth(); 
        $FirstOfLastMonthFormated =$FirstOfLastMonth->format('Ymd'); 
        // get end of last month
        $EndOfLastMonth  = Verta::now()->subMonth()->endMonth();                                                                     ;
        // finih action when month ended
        if($FirstOfLastMonth != $EndOfLastMonth)
        {
            for ($i=1; $i <= 5; $i++)
            {
                // get count of days in week
                $weekDaysLast = 7 -($week_days[$FirstOfLastMonth->toCarbon()->dayName]);
                for ($j=0; $j < $weekDaysLast ; $j++) 
                { 
                    $b_archive = DB::table('b_archive')
                        ->whereBetween('status' , [2,4])
                        ->where('ds' , $FirstOfLastMonthFormated)
                        ->select('price');

                    $amount_last_day[] = DB::table('archive')
                        ->unionAll($b_archive)
                        ->whereBetween('status' , [2,4])
                        ->where('ds' , $FirstOfLastMonthFormated)
                        ->select('price')
                        ->sum('price');
                    // add one day
                    $FirstOfLastMonth->addDay(1);
                    $FirstOfLastMonthFormated  =  $FirstOfLastMonthFormated +1;
                }
                // sum amounts of one by one weeks
                $overalMonthLast[] = array_sum($amount_last_day); 
                $amount_last_day= null;
            }
        }

        // overal info for now month
        $date_month = $TomorrowFormated;
        for ($i=1; $i <= $interval; $i++)
        {
            $date_month  =  $date_month -1;
            $overalMonthNow[] = DB::table('archive')
                ->whereBetween('status' , [2,4])
                ->where('ds' ,  $date_month)
                ->sum('price');
        }


        // get finish carry in mounth
        $date_Carry = $FirstOfMonthFormated;
        for ($i=1; $i <= $interval ; $i++) { 
            $carryInMonth[] = DB::table('havale')
            ->where('status' , 4)
            ->where('dd' , $date_Carry)
            ->count();
            $date_Carry = $date_Carry + 1;  
        }

        // send data to view
        return view('Reports.FinancialReport.driversReport' , 
        [
            'amounts'=>$amounts , 
            'period'=>$period , 
            'amountsLast'=>$overalMonthLast,
            'carryInMonth'=>array_sum($carryInMonth),
            'overalMonthNow'=> array_sum($overalMonthNow), 
            'overalMonthLast'=> array_sum($overalMonthLast),
        ]);
    }
}
