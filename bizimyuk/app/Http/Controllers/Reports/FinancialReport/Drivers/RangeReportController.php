<?php

namespace App\Http\Controllers\Reports\FinancialReport\Drivers;

use Illuminate\Http\Request;
use Hekmatinasser\Verta\Verta;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class RangeReportController extends Controller
{
    public function rangeOfDateReport(Request $request)
    {
        // get start and end of range 
        $startDate =str_replace("/", "", $request->startDate);
        $endDate = str_replace("/", "", $request->endDate);
        // get editable start and end dates
        $startDateEditable = Verta::parseFormat('Y n j', $startDate);
        $endDateEditable =  Verta::parseFormat('Y n j', $endDate);
        // get count of day in start and end date 
        $interval = $startDateEditable->diffDays($endDateEditable);

        // sum of price in dates , day by day 
        for ($i=0; $i <= $interval; $i++) 
        { 
            $b_archive = DB::table('b_archive')
                ->whereBetween('status' , [2,4])
                ->where('ds' , $startDateEditable->format('Ymd'))
                ->select('price');
            $amounts[] = DB::table('archive')
                ->unionAll($b_archive)
                ->whereBetween('status' , [2,4])
                ->where('ds' ,  $startDateEditable->format('Ymd'))
                ->sum('price');

            $startDateEditable->addDay(1);
        }

        // reset start date
        $startDateEditable = Verta::parseFormat('Y n j', $startDate);
        // get period of range
        for ($i=0; $i <= $interval; $i++) { 
            $period [] = $startDateEditable->format('Y-n-j');
            $startDateEditable->addDay(1);
        }
    
        // send data to view
        return view('Reports.FinancialReport.driversReport' , 
        [
            'amounts'=>$amounts , 
            'period'=>$period , 
        ]);

    }
}
