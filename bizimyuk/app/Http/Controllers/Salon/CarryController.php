<?php

namespace App\Http\Controllers\Salon;

use App\Helper\Helper;
use Illuminate\Http\Request;
use Hekmatinasser\Verta\Verta;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CarryController extends Controller
{
    // carry_page
    public function showCarry()
    {
        // get dateS
        $firstOfMounth = Verta::now()->startMonth()->format('Ymd');
        $dateNow =  Verta::now()->format('Ymd');
        
        // get today status orders /get needs record
        $orders = DB::table('havale')
            ->whereBetween('havale.status' , [2,5])
            ->where('havale.dd' ,'<=', $dateNow )
            ->orderBy('havale.status')
            ->select('havale.status' , 'havale.tonaz' , 'havale.barname' , 'havale.ttime' , 'havale.ddate' , 'havale.value_ts' , 'havale.packagekala' , 'havale.company' , 'havale.target' , 'havale.source' , 'havale.phone' , 'havale.name' , 'havale.pelak' , 'havale.nobat' , 'havale.rahgiri')
            ->get();

        // get sum of prices today
        $amount = DB::table('archive')
            ->whereBetween('status' , [2,4])
            ->where('ds' ,'<=', $dateNow )
            ->sum('price');

        // get finish carry in mounth
        $date_Carry = $firstOfMounth;
        $interval = Verta::now()->startMonth()->diffDays(Verta::tomorrow());
        for ($i=1; $i < $interval ; $i++) { 
            $finish[] = DB::table('havale')
            ->where('status' , 4)
            ->where('dd' , $date_Carry)
            ->count();
            $date_Carry = $date_Carry + 1;  
        }

        // show overall info of navgans
        $navgans = DB::table('navgan')
            ->selectRaw('id_vasile,COUNT(*) AS navgan_count')
            ->join('havale', 'havale.phone' , '=' , 'navgan.reg_phone')
            ->where('havale.dd' ,'<=', $dateNow )
            ->groupBy('id_vasile')
            ->get();

        return view('Salon.carry' , 
        [
            'orders' => $orders , 
            'amount' =>$amount , 
            'finish'=>array_sum($finish), 
            'navgans'=>$navgans,
        ]);
    }
}
