<?php

namespace App\Http\Controllers\Salon;

use Carbon\Carbon;
use App\Helper\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Hekmatinasser\Verta\Facades\Verta;

class TurnningController extends Controller
{
    // turnning_page_show
    public function showTurnning()
    {    
         // به علت تستی بودن فیلتر زمان کامنت شده در صورت نیاز از حالت کامنت خارج کنید
        // get date now
        // $dateNow =  Verta::now()->formatJalaliDate();
        
        // get now orders for drivers that want turning
        $orders = DB::table('havale')
            ->where('status' , 100)
            // ->Where('ddate' , $dateNow)
            ->orderBy('nobat')
            ->select('havale.status' , 'havale.tonaz' , 'havale.barname' , 'havale.ttime' , 'havale.ddate' , 'havale.value_ts' , 'havale.packagekala' , 'havale.company' , 'havale.target' , 'havale.source' , 'havale.phone' , 'havale.name' , 'havale.pelak' , 'havale.nobat' , 'havale.rahgiri' , 'havale.id_archive')
            ->get();

        // get sum of prices
        $amount = DB::table('archive')
            ->where('status' , 100)
            ->sum('price');
    
        return view('Salon.turnning' , ['orders' => $orders  , 'amount' =>$amount]);
    }



    // set_turnning
    public function confirmTurnning($archive_id)
    {
        // get_data
        $currentHavale = DB::table('havale')->where('id_archive',$archive_id)->where('status' , 100)->first();
        $lastArchive = DB::table('archive')->where('main_rahgiri', $currentHavale->rahgiri )->whereBetween('status', [2,4])->where('nobat' , ">" , 0)->orderBy('nobat', 'desc')->first();
        $mainBar = DB::table('mainbar')->where('rahgiri',$currentHavale->rahgiri)->first();
        $firstValueBar = $mainBar->value_aval;

        // dateTime
        $time_now = Carbon::now()->format('H:i');
        $date_now =  Verta::now()->format('Y/m/d');  

        // first_turnning
        if (is_null($lastArchive)) {

            DB::table('archive')->where('id', $archive_id)->update([

                'date_step2' => $date_now,
                'time_step2' => $time_now,
                'status' => 2,
                'nobat' => 1,
                'time_control' => $time_now,
                'tc'=>Carbon::now()->format('Hi'),
            ]);

            DB::table('havale')->where('id_archive', (string)$archive_id)->update([
                'nobat' => 1,
                'status' => 2,
            ]);

            DB::table('mainbar')->where('rahgiri',$currentHavale->rahgiri)->update([
                'value_mohasebe' => ($mainBar->value_mohasebe)-1,
            ]);

            return response()->json(['msg'=>"success"]);
        }


        // ofter_first_turnning
        // get_turn
        $nobat = $lastArchive->nobat;
        $nobat++;

        // If the order is larger than the compony request
        if ($nobat > $firstValueBar){

            DB::table('archive')->where('rahgiri', (string)$mainBar->rahgiri)->update([

                'status' => 5,
                'closing' => 1
            ]);
    
            DB::table('havale')->where('rahgiri', (string)$mainBar->rahgiri)->update([

                'status' => 5,
            ]);

            return response()->json(['msg'=>"cancel"]);

        }
        // If the order is smaller than the compony request
        else
        {
            // How storage turns
            if ($nobat < 10)
            {
                $nobat = '00' . $nobat;
            }
            elseif ($nobat >= 10 && $nobat < 100)
            {
                $nobat = '0' . $nobat;
            }
            elseif ($nobat >= 100)
            {
                $nobat =  strval($nobat);
            }
            
            // save_turns
            DB::table('archive')->where('id', $archive_id)->update([
    
                'date_step2' => $date_now,
                'time_step2' => $time_now,
                'status' => 2,
                'nobat' => $nobat,
                'time_control' => $time_now,
                'tc'=>Carbon::now()->format('Hi'),
    
            ]);
    
            DB::table('havale')->where('id_archive', (string)$archive_id)->update([
                'nobat' => $nobat,
                'status' => 2,
            ]);

            DB::table('mainbar')->where('rahgiri',$currentHavale->rahgiri)->update([
                'value_mohasebe' => ($mainBar->value_mohasebe)-1,
            ]);

            return response()->json(['msg'=>"success"]);
        }
     
    }



    
    // cancel_turnning
    public function cancelTurnning($archive_id)
    {

        DB::table('archive')->where('id', $archive_id)->update([

            'status' => 5,
            'closing' => 1,
            'reset'=>1,
        ]);

        DB::table('havale')->where('id_archive', (string)$archive_id)->update([

            'status' => 5,
        ]);

        return response()->json(['msg'=>"cancel"]);

    }
}
