<?php

namespace App\Http\Controllers\Salon;

use Carbon\Carbon;
use App\Helper\Helper;
use Illuminate\Http\Request;
use Hekmatinasser\Verta\Verta;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class WaittingQueueController extends Controller
{
    // waintting_queue
    public function showWaittingQueue()
    {
        // get date now
        $dateNow =  Verta::now()->format('Ymd');
        $timeNow =  Carbon::now()->format('Hi');

        // get lowest data of database that we need
        $loads =  DB::table('mainbar')
            ->join('bar','mainbar.id','=','bar.mainbar_index')
            ->join('typecar','bar.id_vasile','=','typecar.id')
            ->join('typeload','bar.id_load','=','typeload.id')
            ->join('typetkh','bar.id_tkh','=','typetkh.id')
            ->where('mainbar.datebar' ,'<=', $dateNow)
            ->orderBy('mainbar.value_mohasebe','desc')
            ->select('mainbar.te','mainbar.rahgiri','mainbar.source','mainbar.destinition','mainbar.ellam' , 'mainbar.datebar' , 'mainbar.time_start' , 'mainbar.time_end' , 'mainbar.packagekala' , 'mainbar.value_mohasebe' , 'mainbar.id_mohasebe' , 'mainbar.id_kerayeh' , 'mainbar.id_pardakht' , 'mainbar.price','typecar.name as TypeVasile','typeload.name as TypeLoad','typetkh.name as TypeTakh')
            ->get()
            ->map(function($item){
                return collect($item)->toArray();
            });

        return view('Salon.waittingQueue' , ['loads' => $loads , 'timeNow' => $timeNow]);
    }
}
