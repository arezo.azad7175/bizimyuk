<?php

namespace App\Http\Controllers\Companies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompaniesController extends Controller
{
    public function showCompanies()
    {
        $companies = DB::table('profile')
        ->where('id_mojaves' ,'>', 0)
        ->where('allow' , 1 )
        ->get();

        return view('Companies.companies' , ['companies' => $companies]);
    }
}
