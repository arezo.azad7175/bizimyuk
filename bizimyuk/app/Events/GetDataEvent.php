<?php

namespace App\Events;

use App\Events\GetDataEvent;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GetDataEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     */
    protected $message;
    
    public function __construct($data)
    {
        $this->message = $data;
    }

    public function handle(GetDataEvent $event)
    {
      return $event->message;  
    }
    
    public function broadcastWith()
    {
        return[
            'message'=>$this->message
        ];
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): array
    {
        return ['DataChannel'];
    }

    public function broadcastAs()
    {
        return 'GetData_Event';
    }


}
