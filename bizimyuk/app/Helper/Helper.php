<?php

namespace App\Helper;

use Cryptommer\Smsir\Smsir;
use App\Events\GetDataEvent;
use Illuminate\Support\Facades\Http;

class Helper
{

    // send_code_sms
    public static function sendSms($mobile, $code)
    {
        $parameter = new \Cryptommer\Smsir\Objects\Parameters("CODE",(string)$code);
        $parameters = array($parameter);

        $send = smsir::Send();
        $send->Verify((string)$mobile,696093, $parameters);
    }

    // user_pass_send
    public static function sendInfoSms($mobile, String $username , String $newpass)
    {
        $Value_1 =new \Cryptommer\Smsir\Objects\Parameters("V1",(string)$username);    
        $Value_2 =new \Cryptommer\Smsir\Objects\Parameters("V2",(string)$newpass);   
        $parameters = array($Value_1 , $Value_2);

        $send = smsir::Send();
        $send->Verify((string)$mobile,562618, $parameters);
    }

    // send activeDriver sms
    public static function activeDriver($phone,$fullName)
    {
        $parameter = new \Cryptommer\Smsir\Objects\Parameters("name",(string)$fullName);
        $parameters = array($parameter);

        $send = smsir::Send();
        $send->Verify((string)$phone,490612, $parameters);
    }

    // send deactiveDriver sms
    public static function deactiveDriver($phone, $fullName, $deactivateReason)
    {
        $Value_1 =new \Cryptommer\Smsir\Objects\Parameters("name",(string)$fullName);    
        $Value_2 =new \Cryptommer\Smsir\Objects\Parameters("reason",(string)$deactivateReason);   
        $parameters = array($Value_1 , $Value_2);

        $send = smsir::Send();
        $send->Verify((string)$phone,359324, $parameters);
    }


    // captcha_check
    public static function check_captcha()
    {
        if (request()->getMethod() == 'POST') 
        {
            $rules = ['captcha' => 'required|captcha'];
            $validator = validator()->make(request()->all(), $rules);
            if ($validator->fails()) 
            {
                return false;
            } 
            else 
            {
                return true;
            }
        }
    }  
}