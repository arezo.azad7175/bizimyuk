


<?php $__env->startSection('content'); ?>


   <form class="login100-form validate-form" method="post" action="<?php echo e(route('login.checkCode' , session()->get('user_id'))); ?>">
	    
        <?php echo csrf_field(); ?> 

		<span class="login100-form-title">
			کد&nbsp;&nbsp; ارسالی &nbsp;&nbsp; باری رو
		</span>

		<div class="wrap-input100 validate-input" data-validate = "Password is required">
			<input class="input100" type="password" name="pass" placeholder="کد ارسالی را وارد کنید">
			<span class="focus-input100"></span>
			<span class="symbol-input100">
				<i class="fa fa-lock" aria-hidden="true"></i>
			</span>
		</div>
		


		<div class="wrap-input100 manage-code">
		   <div class="timer">
                <p id="countdown"></p>
            </div>

			<div class="refresh-code">
				<a class="restart-timer" href="<?php echo e(route('sendCode' , session()->get('user_id'))); ?>"><i class="fa fa-refresh" aria-hidden="true">&nbsp;&nbsp;ارسال مجدد</i></a>
        	</div>
			
		</div>

		<?php if(Session::has('loginError')): ?>
            <div>
                <p class="error"><?php echo e(Session::get('loginError')); ?> . </p>
            </div>
        <?php endif; ?>

		<div class="container-login100-form-btn">
			<button class="login100-form-btn">
				ورود 
			</button>
		</div>

		<div class="text-center p-t-12">
			<span class="txt1">
				بازگشت به
			</span>
			<a class="txt2" href="<?php echo e(route('login')); ?>">
				صفحه ورود
			</a>
		</div>

	</form>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('Auth.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\bizimyuk.ir\resources\views/Auth/sendLoginCode.blade.php ENDPATH**/ ?>