

<!DOCTYPE html>

<html lang="en">

<head>

	<title>باربری</title>

	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="icon" type="image/x-icon" href="<?php echo e(asset('images/logo.png')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')); ?>">

	

	<!-- css_links -->

	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/Auth/template/bootstrap/bootstrap.min.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/Auth/template/animate/animate.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/Auth/template/hamburgers/hamburgers.min.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/Auth/template/select/select.min.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/Auth/template/main.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/Auth/template/util.css')); ?>">

	

</head>

<body>

	

	<div class="limiter">

		<div class="container-login100">

			<div class="wrap-login100">

				<div class="login100-pic js-tilt" data-tilt>

					<img src="images/img-01.png" alt="IMG">

				</div>









                <?php echo $__env->yieldContent('content'); ?>

                







				<div class="text-center p-t-70 p-l-42">

					<p class="txt2">

					    تلفن تماس پشتیبانی باربری  33445566-024	

						<i class="fa fa-phone m-l-5" aria-hidden="true"></i>

                        <br>	

						<a href="#" >WWW.BARBARI.IR</a>

					</p>

				</div>

			</div>

		</div>

	</div>

	

	



	<!-- js_links -->



	<script src="<?php echo e(asset('js/Auth/timer.js')); ?>"></script>

	<script src="<?php echo e(asset('js/Auth/template/jquery/jquery-3.2.1.min.js')); ?>"></script>

	<script src="<?php echo e(asset('js/Auth/template/bootstrap/popper.js')); ?>"></script>

	<script src="<?php echo e(asset('js/Auth/template/bootstrap/bootstrap.min.js')); ?>"></script>

	<script src="<?php echo e(asset('js/Auth/template/select/select.min.js')); ?>"></script>

	<script src="<?php echo e(asset('js/Auth/template/tilt/tilt.jquery.min.js')); ?>"></script>

	<script src="<?php echo e(asset('js/Auth/template/main.js')); ?>"></script>



	

</body>

</html><?php /**PATH D:\bizimyuk.ir\resources\views/Auth/layout/master.blade.php ENDPATH**/ ?>