


<?php $__env->startSection('content'); ?>
   <form class="login100-form validate-form" method="post" action="<?php echo e(route('forget.checkUser')); ?>">
      <?php echo csrf_field(); ?>
		<span class="login100-form-title">
			فراموشی&nbsp;&nbsp; رمز&nbsp;&nbsp; باری رو
		</span>

		<div class="wrap-input100 validate-input" data-validate = "phone number is required">
			<input class="input100" type="text" oninput= "this.value = this.value.replace(/[^0-9]/g , '')" name="mobile" placeholder="شماره تماس " maxlength ="11">
			<span class="focus-input100"></span>
			<span class="symbol-input100">
				<i class="fa fa-phone m-l-5" aria-hidden="true"></i>
			</span>

		</div>
		
		<!-- captcha -->
		<div class="wrap-input100 validate-input" data-validate = "captcha is required">
			<input class="input100" type="text" name="captcha" placeholder="کد زیر را وارد کنید">
			<span class="focus-input100"></span>
			<span class="symbol-input100">
				<i class="fa fa-qrcode" aria-hidden="true"></i>
			</span>
		</div>


		<div class="wrap-input100 captcha">
			<img src="<?php echo e(captcha_src()); ?>" alt="">		
			<i class="fa fa-refresh" aria-hidden="true"></i>
		</div>

		<?php if(Session::has('forgetError')): ?>
			<div>
				<p class="error"><?php echo e(Session::get('forgetError')); ?> . </p>
			</div>
    	<?php endif; ?>
		
		<div class="container-login100-form-btn">
			<button class="login100-form-btn">
				ارسال رمز موقت
			</button>
		</div>

		<div class="text-center p-t-12">
			<span class="txt1">
				بازگشت به
			</span>
			<a class="txt2" href="<?php echo e(route('login')); ?>">
				صفحه ورود
			</a>
		</div>

	</form>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Auth.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\bizimyuk.ir\resources\views/Auth/forgetPass.blade.php ENDPATH**/ ?>