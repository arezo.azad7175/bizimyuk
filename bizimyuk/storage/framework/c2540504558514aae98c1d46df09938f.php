<?php $__env->startSection('content'); ?>

<section class="mini" id="work-process">
    <div class="mini-content">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                   <div class="info">
                        <h1>سالن نوبت دهی</h1>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="turnning-table">
                    <table>
                            <thead>
                            <tr>
                                <td> تناژ</td>
                                <td> شماره بارنامه</td>
                                <td> ساعت</td>
                                <td>تاریخ اعلامی</td>
                                <td>مانده</td>
                                <td>محاسبه</td>
                                <td>مشخصات بار اعلامی</td>
                                <td>شرکت حمل و نقل</td>
                                <td>مقصد بار</td>
                                <td>مبدا بار</td>
                                <td>موبایل راننده</td>
                                <td>مشخصات راننده</td> 
                                <td>مشخصات ناوگان</td> 
                                <td>نوبت بارگیری</td> 
                                <td>رهگیری</td> 
                                <td>وضعیت حمل</td>  
                            </tr>
                            </thead>
                        <!-- show carry fields -->
                        <tbody>
                            
                        <?php if($orders): ?>

                            <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <!-- waiting for loading -->
                                <?php if($order->status === 2): ?>
                                <tr style= "background-color: #dddd4e;">
                                    <td> <?php echo e($order->tonaz); ?> </td>
                                    <td> <?php echo e($order->barname); ?> </td>
                                    <td> <?php echo e($order->ttime); ?> </td>
                                    <td> <?php echo e($order->ddate); ?></td>
                                    <td><?php echo e($order->value_ts); ?></td>
                                    <td>سرویس</td>
                                    <td> <?php echo e($order->packagekala); ?></td>
                                    <td> <?php echo e($order->company); ?></td>
                                    <td> <?php echo e($order->target); ?></td>
                                    <td> <?php echo e($order->source); ?></td>
                                    <td> <?php echo e($order->phone); ?></td>
                                    <td> <?php echo e($order->name); ?></td>
                                    <td> <?php echo e($order->pelak); ?></td>
                                    <td> <?php echo e($order->nobat); ?> </td>
                                    <td> <?php echo e($order->rahgiri); ?> </td> 
                                    <td> در انتظار بارگیری</td> 
                                </tr>
                                <?php endif; ?>


                                <!-- Waiting for unloading  -->
                                <?php if($order->status === 3): ?>
                                <tr style= "background-color: #84cbdb;">
                                    <td> <?php echo e($order->tonaz); ?> </td>
                                    <td> <?php echo e($order->barname); ?> </td>
                                    <td> <?php echo e($order->ttime); ?> </td>
                                    <td> <?php echo e($order->ddate); ?></td>
                                    <td><?php echo e($order->value_ts); ?></td>
                                    <td>سرویس</td>
                                    <td> <?php echo e($order->packagekala); ?></td>
                                    <td> <?php echo e($order->company); ?></td>
                                    <td> <?php echo e($order->target); ?></td>
                                    <td> <?php echo e($order->source); ?></td>
                                    <td> <?php echo e($order->phone); ?></td>
                                    <td> <?php echo e($order->name); ?></td>
                                    <td> <?php echo e($order->pelak); ?></td>
                                    <td> <?php echo e($order->nobat); ?> </td>
                                    <td> <?php echo e($order->rahgiri); ?> </td> 
                                    <td> در انتظار تخلیه</td> 
                                </tr>
                                <?php endif; ?>


                                <!-- End of operation -->
                                <?php if($order->status === 4): ?>
                                <tr style= "background-color: #84db8b;">
                                    <td> <?php echo e($order->tonaz); ?> </td>
                                    <td> <?php echo e($order->barname); ?> </td>
                                    <td> <?php echo e($order->ttime); ?> </td>
                                    <td> <?php echo e($order->ddate); ?></td>
                                    <td><?php echo e($order->value_ts); ?></td>
                                    <td>سرویس</td>
                                    <td> <?php echo e($order->packagekala); ?></td>
                                    <td> <?php echo e($order->company); ?></td>
                                    <td> <?php echo e($order->target); ?></td>
                                    <td> <?php echo e($order->source); ?></td>
                                    <td> <?php echo e($order->phone); ?></td>
                                    <td> <?php echo e($order->name); ?></td>
                                    <td> <?php echo e($order->pelak); ?></td>
                                    <td> <?php echo e($order->nobat); ?> </td>
                                    <td> <?php echo e($order->rahgiri); ?> </td> 
                                    <td> پایان عملیات</td> 
                                </tr>
                                <?php endif; ?>


                                <!-- cancel load -->
                                <?php if($order->status === 5): ?>
                                <tr style= "background-color: #fd4646;">
                                    <td> <?php echo e($order->tonaz); ?> </td>
                                    <td> <?php echo e($order->barname); ?> </td>
                                    <td> <?php echo e($order->ttime); ?> </td>
                                    <td> <?php echo e($order->ddate); ?></td>
                                    <td><?php echo e($order->value_ts); ?></td>
                                    <td>سرویس</td>
                                    <td> <?php echo e($order->packagekala); ?></td>
                                    <td> <?php echo e($order->company); ?></td>
                                    <td> <?php echo e($order->target); ?></td>
                                    <td> <?php echo e($order->source); ?></td>
                                    <td> <?php echo e($order->phone); ?></td>
                                    <td> <?php echo e($order->name); ?></td>
                                    <td> <?php echo e($order->pelak); ?></td>
                                    <td> <?php echo e($order->nobat); ?> </td>
                                    <td> <?php echo e($order->rahgiri); ?> </td> 
                                    <td>  لغو بار</td> 
                                </tr>
                                <?php endif; ?>
                                
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                        <?php endif; ?>
                        </tbody>
                    
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Dahboard.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/bariroback/domains/bizimyuk.ir/resources/views/Salon/carry.blade.php ENDPATH**/ ?>