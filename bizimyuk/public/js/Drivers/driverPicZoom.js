
var magnifying_areas = document.querySelectorAll("#magnifying_area");

magnifying_areas.forEach(function(magnifying_area) {

  var magnifying_img = magnifying_area.querySelector("img");
  window.addEventListener('resize' , ()=>{
    
    if(window.innerWidth >= 500){
      magnifying_area.addEventListener("mousemove", function(event) {
        var clientX = event.clientX - magnifying_area.offsetLeft;
        var clientY = event.clientY - magnifying_area.offsetTop;
        var mWidth = magnifying_area.offsetWidth;
        var mHeight = magnifying_area.offsetHeight;
        clientX = (clientX / mWidth) * 75;
        clientY = (clientY / mHeight) * 75;
        magnifying_img.style.transform =
          "translate(-" + clientX + "%, -" + clientY + "%) scale(2)";
      });
    
      magnifying_area.addEventListener("mouseleave", function() {
        magnifying_img.style.transform = "translate(-50%,-50%) scale(1)";
      });
    }
  })

});