
var table = document.getElementById("table");
var searchInput = document.getElementById("search");
var resultsList = document.getElementById("results");
var debounceTimer;


// search drivers outomaticly
function filterRows() {
  var input = searchInput.value.toLowerCase();
  var rows = table.querySelectorAll("tbody tr");
  var matchingRows = [];

  for (var i = 0; i < rows.length; i++) {
    var row = rows[i];
    var matchFound = false;

    for (var j = 0; j < row.cells.length; j++) {
      var cell = row.cells[j];
      var text = cell.innerHTML.toLowerCase();

      if (text.indexOf(input) > -1) {
        matchFound = true;
        break;
      }
    }

    if (matchFound) {
      matchingRows.push(row);
    }
  }

  for (var i = 0; i < rows.length; i++) {
    var row = rows[i];
    if (matchingRows.includes(row)) {
      row.style.display = "";
    } else {
      row.style.display = "none";
    }
  }
}

searchInput.addEventListener("input", function () {
  clearTimeout(debounceTimer);
  debounceTimer = setTimeout(filterRows, 300);
});

table.addEventListener("click", function (event) {
  if (event.target.tagName === "TR") {
    var row = event.target;
    var li = document.createElement("li");
    li.innerHTML = row.cells[0].innerHTML;
    resultsList.appendChild(li);
  }
});



