
// send sms to driver
function driverSendSms(reason) {
  
    const csrfToken = document.querySelector('meta[name="driver-csrf-token"]').getAttribute('content');
    $.ajax({
        headers: {
          "X-CSRF-Token": csrfToken
      },
      url: '/drivers/sendSms',
      type: 'POST',
      dataType: 'json',
      data: {
        fullName: $("#name").val() + " " + $("#lastName").val(),
        phone: $("#phone").val(),
        status:$("#status").val(),
        reason:reason,
      },
      success: function(response) {
        window.location.reload();
      },
    });
  }
  