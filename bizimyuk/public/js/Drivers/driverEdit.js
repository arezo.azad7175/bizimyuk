
//reason box status
$('#status').on('change',function(e){
    $('#reason').prop('disabled',parseInt($(e.target).val()) == 1);
  });
  
// get persion date input
$('.date-input').persianDatepicker({
    observer: true,
    format: 'YYYY/MM/DD',
    altField: '.observer-example-alt'
});