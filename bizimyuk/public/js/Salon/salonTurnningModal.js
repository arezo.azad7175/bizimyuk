// turning_modal
var archive_id;
const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

// modal_user_name-turnningSalon
function orders(id,name)
{
  archive_id = id;
  document.querySelector(".modal-user-name").innerText = name;
}


// Confirm function-turnningSalon
function confirmAction() {
    $.ajax({
      url: '/salon/turnning/confirm/' + archive_id ,
      type: 'POST',
      dataType: 'json',
      data: {
        _token: csrfToken //  CSRF token 
      },
      success: function(response) {
        window.location.reload();
      },
    });
  }
  


  // Cancel function-turnningSalon
  function cancelAction() {
    $.ajax({
      url: '/salon/turnning/cancel/' + archive_id ,
      type: 'POST',
      dataType: 'json',
      data: {
        _token: csrfToken // Add CSRF 
      },
      success: function(response) {
        window.location.reload();
      },
    });
  }