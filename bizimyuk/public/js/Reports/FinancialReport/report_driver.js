// show chart
let ctx = document.getElementById("myChart").getContext("2d");
var myChart = new Chart(ctx, {
    type: "line",
    data: {
        labels: period,
        datasets: [
            {
                label:"جاری",
                pointRadius: 5,
                data: amounts,
                borderColor:  "#1da31d",
                pointBackgroundColor: "#6d6dff",
                pointBorderColor: "#6d6dff",
            },
        ],
    },

    options: {
        legend: {
            display:true,
            labels: {
                fontColor: "white",
                fontSize: 18,
            }
        } ,          
        scales: {
            yAxes: [{
                ticks: {
                    callback:function(label, index, labels) {
                        return "  "+label.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" تومان";
                    },
                    fontColor: "white",
                    fontSize: 14,
                    beginAtZero: true
                }
            }],
            xAxes: [{
                ticks: {
                    fontColor: "white",
                    fontSize: 14,
                    stepSize: 1,
                    beginAtZero: true,
                }
            }]
        }
    }
});

// update chart for compare
function updateChartData($amounts ,$amountsLast)
{
    let datasets=[];
    // set current amounts 
    let firstDataset=myChart.config.data.datasets[0];
    firstDataset.data = $amounts;
    datasets[0]=firstDataset;

    // set last amounts
    if($amountsLast){
        datasets[1]={
            label:"گذشته",
            pointRadius: 5,
            data: $amountsLast,
            borderColor:  "red",
            pointBackgroundColor: "#6d6dff",
            pointBorderColor: "#6d6dff",
        }
    }
    myChart.config.data.datasets=datasets;

    myChart.update();
}

