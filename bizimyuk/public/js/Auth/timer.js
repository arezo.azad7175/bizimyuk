
var startMinute = 60;
const  countDownEvent = document.getElementById('countdown');
var timer;
timer=setInterval(startTimer , 1000);
const csrfToken = document.querySelector('meta[name="csrf-token-code"]').getAttribute('content');

// Confirm function-turnningSalon
function sendCodeAgain() {
    // timer-set
    if(startMinute < 1)
    {
        startMinute=60;
        clearInterval(timer);
        timer=setInterval(startTimer , 1000);
    }

    // send-code
    $.ajax({
      url: '/sendCode' ,
      type: 'POST',
      dataType: 'json',
      data: {
        _token: csrfToken //  CSRF token 
      },
      success: function(response) {
        location.reload();
      },
    });
  }


function startTimer() 
{
  // start-timer
    if(startMinute > 0)
    {
        startMinute--;
        const pretty = startMinute > 9 ? startMinute : '0'+startMinute;
        countDownEvent.innerHTML = pretty;

        // send-again button deactive
        document.getElementById('sendCode').style.display='none';
    }
    // end-timer
    else
    {
        clearInterval(timer);
        countDownEvent.innerHTML = '';

        // send-again button active
        document.getElementById('sendCode').style.display='block';
    }
}



