@extends('Layout.master')
@section('content')

<!-- scroll hidden in this page -->
<style>
    body {overflow: hidden}
</style>

<section class="mini" id="work-process">
        <div class="mini-content">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-3 col-lg-6">
                        <!-- title -->
                        <div class="info">
                            <h1>میزکار باربری </h1>
                        </div>
                    </div>
                </div>

                <!-- menu-options -->
                <div class="row">
                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="{{route('companies')}}" class="mini-box">
                            <i class="icofont-chart-flow-1"></i>
                            <strong>شرکت ها </strong>
                            <span>مشاهده و ویرایش اطلاعات شرکت ها</span>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="{{route('drivers')}}" class="mini-box">
                            <i class="icofont-users-social"></i>
                            <strong>رانندگان</strong>
                            <span>مشاهده و ویرایش اطلاعات رانندگان</span>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="#" class="mini-box">
                            <i class="icofont-fast-delivery" style="color:#cd0000"></i>
                            <strong>سالن رزرو</strong>
                            <span>مشاهده بارهای رزرو شده</span>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="{{route('waittingQueue')}}" class="mini-box">
                            <i class="icofont-delivery-time"></i>
                            <strong>سال انتظار</strong><span>
                            <span>مشاهده بارهای در صف رانندگان</span>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="{{route('carry')}}" class="mini-box">
                            <i class="icofont-articulated-truck"></i>
                            <strong>سالن حمل</strong>
                            <span>مشاهده رانندگان در حال حمل</span>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="{{route('turnning')}}" class="mini-box">
                            <i class="icofont-architecture-alt"></i>
                            <strong>سالن نوبت دهی</strong><span>
                            <span>مشاهده رانندگان در انتظار نوبت</span>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="{{route('financialWeekReport')}}" class="mini-box">
                            <i class="icofont-money"></i>
                            <strong>گزارش مالی </strong><span>
                            <span>مشاهده و فیلتر بخش مالی</span>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="#" class="mini-box">
                            <i class="icofont-clip-board" style="color:#cd0000"></i>
                            <strong>گزارش ماهانه</strong><span>
                            <span>مشاهده و فیلتر بارهای ماهانه</span>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="#" class="mini-box">
                            <i class="icofont-clip-board" style="color:#cd0000"></i>
                            <strong>گزارش هفتگی</strong><span>
                            <span>مشاهده و فیلتر بارهای هفتگی</span>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="#" class="mini-box">
                            <i class="icofont-clip-board" style="color:#cd0000"></i>
                            <strong>گزارش روزانه</strong><span>
                            <span>مشاهده و فیلتر بارهای روزانه</span>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="#" class="mini-box">
                            <i class="icofont-chart-histogram" style="color:#cd0000"></i>
                            <strong>گزارش گیری کلی</strong><span>
                            <span>مشاهده و فیلتر بارهای خاص</span>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="#" class="mini-box">
                            <i class="icofont-ui-clip-board" style="color:#cd0000"></i>
                            <strong>تایید بار</strong><span>
                            <span>مشاهده بار تایید شده شرکت ها</span>
                        </a>
                    </div>
                </div>

                <!-- overalInfo -->
                <p class="generalInfo">
                    <span class="generalSpan"><span> در انتظار تخلیه:</span><span>{{$carry}}</span><span>سرویس</span></span>
                    <span class="generalSpan"><span> در انتظار بارگیری:</span><span>{{$loading}}</span><span>سرویس</span></span>
                    <span class="generalSpan"><span>در انتظار نوبت:</span><span>{{$turnning}}</span><span>سرویس</span></span>
                    <span class="generalSpan"><span>پایان عملیات :</span><span>{{$finish}}</span><span>سرویس</span></span>
                    <span class="generalSpan"><span> لغو بار :</span><span>{{$cancel}}</span><span>سرویس</span></span>
                </p>
            </div>
        </div>
    </section>
@endsection
