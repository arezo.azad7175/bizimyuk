@extends('Auth.layout.master')

@section('content')

	<form class="login100-form validate-form" method="post" action="{{route('login.check')}}">
	   @csrf
	   <!-- title -->
	   <span class="login100-form-title">
			ورود&nbsp;&nbsp; به&nbsp;&nbsp; باربری 
		</span>

		<!-- username -->
		<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
			<input class="input100" type="text" name="username" placeholder="نام کاربری">
			<span class="focus-input100"></span>
			<span class="symbol-input100">
				<i class="fa fa-user" aria-hidden="true"></i>
			</span>
		</div>

		<!-- password -->
		<div class="wrap-input100 validate-input" data-validate = "Password is required">
			<input class="input100" type="password" name="password" placeholder="رمزعبور">
			<span class="focus-input100"></span>
			<span class="symbol-input100">
				<i class="fa fa-lock" aria-hidden="true"></i>
			</span>
		</div>
	
		<!-- input-captcha -->
		<div class="wrap-input100 validate-input" data-validate = "captcha is required">
			<input class="input100" type="text" name="captcha" placeholder="کد زیر را وارد کنید">
			<span class="focus-input100"></span>
			<span class="symbol-input100">
				<i class="fa fa-qrcode" aria-hidden="true"></i>
			</span>
		</div>

		<!-- captcha-image -->
		<div class="wrap-input100 captcha row">
			<img src="{{captcha_src()}}" onclick="this.src='/captcha/default?'+Math.random()" id="captchaCode" alt="" class="captcha">
			<a rel="nofollow" href="javascript:;" onclick="document.getElementById('captchaCode').src='captcha/default?'+Math.random()" class="refresh"><i class="fa fa-refresh" aria-hidden="true"></i></a>
		</div>

		<!-- errors -->
        @if (Session::has('loginError'))
            <div>
                <p class="error">{{Session::get('loginError')}} . </p>
            </div>
        @endif 

		<!-- back-login-page -->
		<div class="container-login100-form-btn">
			<button class="login100-form-btn">
				ورود
			</button>
		</div>
		
		<!-- go-forgetPass-page -->
		<div class="text-center p-t-12">
			<span class="txt1">
				فراموشی
			</span>
			<a class="txt2" href="{{route('forget.Pass')}}">
				رمزعبور؟
			</a>
		</div>
	</form>

@endsection

			