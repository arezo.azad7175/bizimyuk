@extends('Auth.layout.master')
@section('content')

   <form class="login100-form validate-form" method="post" action="{{route('login.checkCode')}}">
        @csrf 

		<!-- title -->
		<span class="login100-form-title">
			کد&nbsp;&nbsp; ارسالی &nbsp;&nbsp; باری رو
		</span>

		<!-- get-code-input -->
		<div class="wrap-input100 validate-input" data-validate = "Password is required">
			<input class="input100" type="password" name="pass" placeholder="کد ارسالی را وارد کنید">
			<span class="focus-input100"></span>
			<span class="symbol-input100">
				<i class="fa fa-lock" aria-hidden="true"></i>
			</span>
		</div>

		<div class="wrap-input100 manage-code">
			<!-- timer -->
		   <div class="timer">
                <p id="countdown"></p>
            </div>

			<!-- resend-code -->
			<div class="refresh-code">
				<a><button type="button" onclick='sendCodeAgain()' id="sendCode"><i class="fa fa-refresh" aria-hidden="true">&nbsp;&nbsp;ارسال مجدد</i></button></a>
        	</div>
		</div>

		<!-- errors -->
		@if (Session::has('loginError'))
            <div>
                <p class="error">{{Session::get('loginError')}} . </p>
            </div>
        @endif

		<!-- submit-form -->
		<div class="container-login100-form-btn" type="submit">
			<button class="login100-form-btn" >
				ورود 
			</button>
		</div>

		<!-- back-to-login-page -->
		<div class="text-center p-t-12">
			<span class="txt1">
				بازگشت به
			</span>
			<a class="txt2" href="{{route('login')}}">
				صفحه ورود
			</a>
		</div>

	</form>
@endsection