@extends('Auth.layout.master')
@section('content')

<form class="login100-form validate-form" method="post" action="{{route('forget.checkCode' , session()->get('user_id'))}}">
	@csrf
	<!-- title -->
    <span class="login100-form-title">
        فراموشی&nbsp;&nbsp; رمز&nbsp;&nbsp; باری رو
    </span>

	<!-- get-code-input -->
	<div class="wrap-input100 validate-input" data-validate = "sending code is required">
		<input class="input100" type="text" name="pass" placeholder="کد ارسالی">
		<span class="focus-input100"></span>
		<span class="symbol-input100">
			<i class="fa fa-envelope-open" aria-hidden="true"></i>
	    </span>
	</div>

	<div class="wrap-input100 manage-code">
		<!-- timer -->
		<div class="timer">
            <p id="countdown"></p>
        </div>

		<!-- resend-code -->
		<div class="refresh-code">
			<a class="restart-timer" href="{{route('sendCode' , session()->get('user_id') )}}"><i class="fa fa-refresh" aria-hidden="true">&nbsp;&nbsp;ارسال مجدد</i></a>
		</div>
	</div>

	<!-- errors -->
	@if (Session::has('forgetError'))
        <div>
            <p class="error">{{Session::get('forgetError')}} . </p>
        </div>
    @endif

	<div class="container-login100-form-btn">
		<button class="login100-form-btn">
			دریافت رمز عبور و نام کاربری
		</button>
	</div>

</form>
@endsection