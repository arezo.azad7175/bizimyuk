

<!DOCTYPE html>
<html lang="en">
<head>
	<!-- token -->
	<meta name="csrf-token-code" content="{{ csrf_token() }}">
	<!-- title -->
	<title>باربری</title>

	<meta name="viewport" content="width=device-width,height=device-height , initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="width=device-width , height=device-height , initial-scale=1.0">
	<!-- logo -->
	<link rel="icon" type="image/x-icon" href="{{asset('images/logo.png')}}">
	<!-- fonts -->
	<link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">

	<!-- bootstrap links -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

	<!-- css_links -->
	<link rel="stylesheet" type="text/css" href="{{asset('css/Auth/template/animate/animate.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/Auth/template/hamburgers/hamburgers.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/Auth/template/select/select.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/Auth/template/main.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/Auth/template/util.css')}}">

</head>

<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">

				<div class="login100-pic js-tilt" data-tilt>
					<img src="images/img-01.png" alt="IMG">
				</div>

				<!-- diffrent-AuthPart -->
                @yield('content')

				<!-- footer -->
				<div class="text-center p-t-70 p-l-42">
					<p class="txt2">
					    تلفن تماس پشتیبانی باربری  33445566-024	
						<i class="fa fa-phone m-l-5" aria-hidden="true"></i>
                        <br>	
						<a href="#" >WWW.BARBARI.IR</a>
					</p>
				</div>

			</div>
		</div>
	</div>	

	<!-- js_links -->
	<script src="{{asset('js/Auth/timer.js')}}"></script>
	<script src="{{asset('js/Auth/template/jquery/jquery-3.2.1.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
	<script src="{{asset('js/Auth/template/select/select.min.js')}}"></script>
	<script src="{{asset('js/Auth/template/tilt/tilt.jquery.min.js')}}"></script>
	<script src="{{asset('js/Auth/template/main.js')}}"></script>

</body>
</html>