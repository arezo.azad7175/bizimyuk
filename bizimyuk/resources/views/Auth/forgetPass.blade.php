@extends('Auth.layout.master')
@section('content')

   <form class="login100-form validate-form" method="post" action="{{route('forget.checkUser')}}">
      @csrf
		<span class="login100-form-title">
			فراموشی&nbsp;&nbsp; رمز&nbsp;&nbsp; باری رو
		</span>

		<div class="wrap-input100 validate-input" data-validate = "phone number is required">
			<!-- phoneNumber-with-validate -->
			<input class="input100" type="text" oninput= "this.value = this.value.replace(/[^0-9]/g , '')" name="mobile" placeholder="شماره تماس " maxlength ="11">
			<span class="focus-input100"></span>
			<span class="symbol-input100">
				<i class="fa fa-phone m-l-5" aria-hidden="true"></i>
			</span>
		</div>

		<!-- captcha-input -->
		<div class="wrap-input100 validate-input" data-validate = "captcha is required">
			<input class="input100" type="text" name="captcha" placeholder="کد زیر را وارد کنید">
			<span class="focus-input100"></span>
			<span class="symbol-input100">
				<i class="fa fa-qrcode" aria-hidden="true"></i>
			</span>
		</div>

		<!-- captcha-image -->
		<div class="wrap-input100 captcha">
			<img src="{{captcha_src()}}" alt="">		
			<i class="fa fa-refresh" aria-hidden="true"></i>
		</div>

		<!-- errors -->
		@if (Session::has('forgetError'))
			<div>
				<p class="error">{{Session::get('forgetError')}} . </p>
			</div>
    	@endif

		<!-- send-code -->
		<div class="container-login100-form-btn">
			<button class="login100-form-btn">
				ارسال رمز موقت
			</button>
		</div>
		<!-- back-login-page -->
		<div class="text-center p-t-12">
			<span class="txt1">
				بازگشت به
			</span>
			<a class="txt2" href="{{route('login')}}">
				صفحه ورود
			</a>
		</div>
	</form>
	
@endsection