<!DOCTYPE html>
<html lang="en">

<head>

    <!-- get csrf_token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="driver-csrf-token" content="{{ csrf_token() }}">

    <!-- config_page_size -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,height=device-height , initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="width=device-width , height=device-height , initial-scale=1.0">
    
    <!-- set_icon_title -->
    <link rel="icon" type="image/png" href="{{asset('images/logo.png')}}"/>
    <title>باربری</title>

    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/template/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('css/template/templatemo-softy-pinko.css')}}">
    <link rel="stylesheet" href="{{asset('css/salon.css')}}">
    <link rel="stylesheet" href="{{asset('css/drivers.css')}}">
    <link rel="stylesheet" href="{{asset('css/reports.css')}}">
    <link rel="stylesheet" href="{{asset('css/template/flex-slider.css')}}">

    <!-- set_fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('icofont/icofont.css')}}">
    
    <!-- jQuery -->
    <script src="{{asset('js/template/jquery-2.1.0.min.js')}}"></script>

    <!-- bootstrap link -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <!-- persion_datePicker must linked here after jquery -->
    <link rel="stylesheet" href="{{asset('css/persion-datePicker/persian-datepicker.css')}}"/>
    <script src="{{asset('js/persion-datePicker/persian-date.js')}}"></script>
    <script src="{{asset('js/persion-datePicker/persian-datepicker.js')}}"></script>

    <!-- driver pelak -->
    <link href="https://fonts.googleapis.com/css2?family=Dosis:wght@600&family=Markazi+Text:wght@600&display=swap" rel="stylesheet">

</head>
    
<body>
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- change password message -->
                    <!-- success -->
                    @if(Session::has('changeSuccess'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>موفق!</strong> {{Session::get('changeSuccess')}}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif

                    <!-- unsuccessful -->
                    @if(Session::has('changeError'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>نا موفق!</strong> {{Session::get('changeError')}}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif


                    <nav class="main-nav">
                        <!--logo-->
                        <a href="{{route('dashboard')}}" class="logo">
                            <img src="{{asset('images/logo_ico.png')}}" alt="Softy Pinko"/>
                        </a>


                        <!-- Menu Start-->
                        <ul class="nav">

                            <!--for drivers  -->
                            @stack('drivers-filter')
                            
                            <!-- logout -->
                            <li><form action="{{route('logout')}}" method="post">
                                @csrf
                                <a><input type="submit" value="خروج"></a>
                            </form></li>

                            <!-- change password -->
                            <li><a><input type="button" class="btn-changePass" value="تغییر رمز" data-toggle="modal" data-target="#changePassModal"></a></li>

                            <!-- about us -->
                            <li><a href="{{route('aboutUs')}}">درباره ما</a></li>

                            <!-- home -->
                            <li><a href="{{route('dashboard')}}" class="active"><i class="icofont-ui-home"></i></a></li>
                        </ul>
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    <!-- content -->
    @yield('content')


    <!-- js links -->

    <!-- jQuery -->
    <script src="{{asset('js/template/jquery-2.1.0.min.js')}}"></script>

    <!-- bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

    <!-- persion_datePicker must linked here after jquery -->
    <script src="{{asset('js/persion-datePicker/persian-date.js')}}"></script>
    <script src="{{asset('js/persion-datePicker/persian-datepicker.js')}}"></script>
    
    <!-- salon_js -->
    <script src="{{asset('js/Salon/salonTurnningModal.js')}}"></script>

    <!-- Plugins -->
    <script src="{{asset('js/template/scrollreveal.min.js')}}"></script>
    <script src="{{asset('js/template/waypoints.min.js')}}"></script>
    <script src="{{asset('js/template/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('js/template/imgfix.min.js')}}"></script> 

    <!-- Global Init -->
    <script src="{{asset('js/template/custom.js')}}"></script>

    <!-- edit drivers_js -->
    <script src="{{asset('js/Drivers/driverEdit.js')}}"></script>
    <script src="{{asset('js/Drivers/driverPicZoom.js')}}"></script>
    <script src="{{asset('js/Drivers/driverSendSms.js')}}"></script>
    <script src="{{asset('js/Drivers/driverSearchAutomaticly.js')}}"></script>

    <!-- Report -->
    <script src="{{asset('js/Reports/FinancialReport/report_driver.js')}}"></script>
    <script src="{{asset('js/Reports/FinancialReport/report.js')}}"></script>

    <!-- required links part in another pages -->
    @stack('script-link')


    <!-- modal -->
    <!-- for change password -->
    <div class="modal fade" id="changePassModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <!-- title -->
        <div class="modal-header">
            <h5 class="modal-title" id="driverModalLongTitle">ویرایش رمز عبور</h5>
        </div>
        <div class="modal-body changePass">
            <!-- entry form -->
            <form action="{{route('changePass')}}" method="post">
                @csrf
                <input type="password"  name="pass" class="form-control" placeholder="رمزعبور فعلی خود را وارد کنید">
                <input type="password"  name="newPass" class="form-control" placeholder="رمزعبور جدید خود را وارد کنید">
                <input type="password"  name="confirmNewPass" class="form-control" placeholder="رمز جدید خود را دوباره وارد کنید">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> لغو </button>
                    <button type="submit" class="btn btn-success">تایید</button>
                </div>
            </form>
        </div>
    </div>
    </div>
  
</body>
</html>
