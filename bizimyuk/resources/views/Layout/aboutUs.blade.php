@extends('Layout.master')

@section('content')

<section class="mini" id="work-process">
        <div class="mini-content">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-3 col-lg-6">
                        <!-- title -->
                        <div class="info">
                            <h1>درباره ی ما </h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- image -->
                    <div class="col-6 col-lg-6 col-md-12 col-sm-12">
                        <img class="img-about" src="{{asset('images/about.jpg')}}" alt="باربری ایمن">
                    </div>

                    <!-- description -->
                    <div class="col-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="content-about">
                            <p>اگر قصد جا به جایی و اسباب کشی ایمن و مطمئن را دارید دیگر نگران نباشید.</p>
                            <p><strong>شرکت باربری بیزیم یوک</strong> با بیش از یک دهه فعالیت در زمینه اسباب کشی و جا به جایی وسایل با عقد قرارداد رسمی در ابتدای فعالیت با مشتریان عزیز ، خیال ایشان را آسوده می کند. این شرکت با بهترین کادر با تجربه و ماهر که متشکل از راننده های ماهر و کارگران با تجربه است در خدمت شما می باشد.</p>
                            <p><strong>شرکت باربری بیزیم یوک</strong> تمام تلاش خود را کرده است که وسایل حمل و نقل مجهز به ضربه گیرها و پتو های و طناب جهت ایمن سازی وسایل را دارا باشد . این شرکت در هر ساعتی از شبانه روز بدون تعطیلی آماده خدمت رسانی به شما مشتریان گرامی می باشد.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

@endsection