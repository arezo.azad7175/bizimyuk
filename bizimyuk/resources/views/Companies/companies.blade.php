@extends('Layout.master')
@section('content')

<section class="mini" id="work-process">
    <div class="mini-content">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <!-- title -->
                   <div class="info">
                        <h1>شرکت ها</h1>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <!-- tables -->
                    <div class="company-table">
                    <table>
                        <!-- head-part -->
                        <thead>
                        <tr>
                            <td>شارژ</td>
                            <td> آدرس </td> 
                            <td>شماره تماس مدیریت </td> 
                            <td> نام مدیریت </td>  
                        </tr>
                        </thead>
                        <!-- show company fields -->
                        <tbody>
                        @if($companies)
                            @foreach($companies as $company)
                            <tr>
                                <td>{{$company->amount}}</td>
                                <td>{{$company->address}}</td>
                                <td> {{$company->phone}}</td>
                                <td> {{$company->namemodir}} </td>
                            </tr>
                            @endforeach 
                        @endif
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection