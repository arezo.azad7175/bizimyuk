@extends('Layout.master')
@section('content')

<section class="mini" id="work-process">
    <div class="mini-content">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <!-- title -->
                   <div class="info">
                        <h1>سالن حمل بار</h1>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="overallInfo">
                        <!-- overal info part -->
                        <span class="amount"> کل: <span>{{number_format($amount)}}</span> </span>
                        <span class="amount">  تعداد بار رفته در ماه: <span>{{number_format($finish)}}</span> </span>

                        @foreach($navgans as $navgan)
                            @if($navgan->id_vasile == 1)
                                <span class="amount"> وانت: <span>{{$navgan->navgan_count}}</span> </span>
                            @endif
                            @if($navgan->id_vasile == 2)
                                <span class="amount"> نیسان: <span>{{$navgan->navgan_count}}</span> </span>
                            @endif
                            @if($navgan->id_vasile == 3)
                                <span class="amount"> خاور و کامیونت: <span>{{$navgan->navgan_count}}</span> </span>
                            @endif
                            @if($navgan->id_vasile == 4)
                                <span class="amount"> کامیون911: <span>{{$navgan->navgan_count}}</span> </span>
                            @endif
                            @if($navgan->id_vasile == 5)
                                <span class="amount"> تک: <span>{{$navgan->navgan_count}}</span> </span>
                            @endif
                            @if($navgan->id_vasile == 6)
                                <span class="amount"> جفت: <span>{{$navgan->navgan_count}}</span> </span>
                            @endif
                            @if($navgan->id_vasile == 7)
                                <span class="amount"> تریلی: <span>{{$navgan->navgan_count}}</span> </span>
                            @endif
                        @endforeach
                    </div>

                    <div class="carry-table">
                    <!-- table -->
                    <table>
                        <thead>
                        <tr>
                            <td> تناژ</td>
                            <td> شماره بارنامه</td>
                            <td> ساعت</td>
                            <td>تاریخ اعلامی</td>
                            <td>تعداد کل</td>
                            <td>محاسبه</td>
                            <td>مشخصات بار اعلامی</td>
                            <td>شرکت حمل و نقل</td>
                            <td>مقصد بار</td>
                            <td>مبدا بار</td>
                            <td>موبایل راننده</td>
                            <td>مشخصات راننده</td> 
                            <td>مشخصات ناوگان</td> 
                            <td>نوبت بارگیری</td> 
                            <td>رهگیری</td> 
                            <td>وضعیت حمل</td>  
                        </tr>
                        </thead>
  
                        <tbody>
                        <!-- show carry fields -->
                        @if($orders)
                            @foreach($orders as $order)

                                <!-- check_status_color -->
                                @if($order->status === 2)
                                    @php
                                        $color="#e5e576";
                                    @endphp
                                @elseif($order->status === 3)
                                    @php 
                                        $color="#80cddf"
                                    @endphp
                                @elseif($order->status === 4)
                                    @php
                                        $color="#84db8b" 
                                    @endphp
                                @elseif($order->status === 5)
                                    @php
                                        $color="#fd4646" 
                                    @endphp
                                @endif


                                <!-- waiting for loading -->
                                <tr style= "background-color:{{$color}};">
                                    <td> {{$order->tonaz}} </td>
                                    <td> {{$order->barname}} </td>
                                    <td> {{$order->ttime}} </td>
                                    <td> {{$order->ddate}}</td>
                                    <td>{{$order->value_ts}}</td>
                                    <td>سرویس</td>
                                    <td> {{$order->packagekala}}</td>
                                    <td> {{$order->company}}</td>
                                    <td> {{$order->target}}</td>
                                    <td> {{$order->source}}</td>
                                    <td> {{$order->phone}}</td>
                                    <td> {{$order->name}}</td>
                                    <td> {{$order->pelak}}</td>
                                    <td> {{$order->nobat}} </td>
                                    <td> {{$order->rahgiri}} </td> 

                                    <!-- check_status -->
                                    @if($order->status === 2)
                                        <td> در انتظار بارگیری</td> 
                                    @elseif($order->status === 3)
                                        <td> در انتظار تخلیه</td> 
                                    @elseif($order->status === 4)
                                        <td> پایان عملیات</td> 
                                    @elseif($order->status === 5)
                                        <td>  لغو بار</td> 
                                    @endif        
                                </tr> 
                            @endforeach 
                        @endif
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection