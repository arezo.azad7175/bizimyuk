@extends('Layout.master')
@section('content')

<!-- active scrollbar -->
<style>
    body {overflow: auto}
</style>

<section class="mini" id="work-process">
    <div class="mini-content">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <!-- title -->
                   <div class="info">
                        <h1>سالن نوبت دهی</h1>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <!-- overal info -->
                    <p class="amount">مبلغ کل: <span>{{number_format($amount)}}</span> </p>

                    <!-- table -->
                    <div class="turnning-table">
                    <table>
                            <thead>
                            <tr>
                                <td></td> 
                                <td> تناژ</td>
                                <td> شماره بارنامه</td>
                                <td> ساعت</td>
                                <td>تاریخ اعلامی</td>
                                <td>تعداد کل</td>
                                <td>محاسبه</td>
                                <td>مشخصات بار اعلامی</td>
                                <td>شرکت حمل و نقل</td>
                                <td>مقصد بار</td>
                                <td>مبدا بار</td>
                                <td>موبایل راننده</td>
                                <td>مشخصات راننده</td> 
                                <td>مشخصات ناوگان</td> 
                                <td>رهگیری</td> 
                                <td>وضعیت حمل</td>  
                            </tr>
                            </thead>
                        <tbody>

                        <!-- show turnning fields -->
                        @if($orders)
                            @foreach($orders as $order)
                                <tr>
                                    <!-- send_info_with_Ajax_button_onclick -->
                                    <td><button type="button"  class="btn btn-success"  data-toggle="modal" data-target="#TurnningModal" onclick="orders({{ $order->id_archive}},'{{ $order->name }}')">نوبت دهی</button></td> 
                                    
                                    <td> {{$order->tonaz}} </td>
                                    <td> {{$order->barname}} </td>
                                    <td> {{$order->ttime}} </td>
                                    <td> {{$order->ddate}}</td>
                                    <td>{{$order->value_ts}}</td>
                                    <td>سرویس</td>
                                    <td> {{$order->packagekala}}</td>
                                    <td> {{$order->company}}</td>
                                    <td> {{$order->target}}</td>
                                    <td> {{$order->source}}</td>
                                    <td> {{$order->phone}}</td>
                                    <td> {{$order->name}}</td>
                                    <td> {{$order->pelak}}</td>
                                    <td> {{$order->rahgiri}} </td> 
                                    <td> در انتظار تایید</td> 
                                </tr>  
                            @endforeach
                        @endif
                        </tbody>
                    
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- modal -->
<!-- Turnning_Modal -->
<div class="modal fade" id="TurnningModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">وضعیت بار</h5>
      </div>
      <div class="modal-body">
        <!-- show_driver_name -->
        <p>
        مجوز نوبت دهی به <span class="modal-user-name"></span>
        </p>
      </div>
      <!-- send with ajax -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="reload" onclick="cancelAction()">لغو بار </button>
        <button type="button" class="btn btn-success" data-dismiss="reload" onclick="confirmAction()">تایید بار </button>
      </div>
    </div>
  </div>
</div>

@endsection

