@extends('Layout.master')
@section('content')

<!-- active scrollbar -->
<style>
    body {overflow: auto}
</style>

<div class="mini" id="work-process">
    <div class="mini-content">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <!-- title -->
                   <div class="info">
                        <h1>سالن انتظار</h1>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <!-- table -->
                    <div class="carry-table">
                    <table>
                        <thead>
                        <tr>
                            <td>نوع تخلیه</td>
                            <td>نوع اتاق</td> 
                            <td>نوع ناوگان</td> 
                            <td>قیمت اعلامی حمل</td>
                            <td>پرداختی</td>
                            <td>نوع</td> 
                            <td>سرویس</td> 
                            <td>مقدار مانده بار</td>
                            <td>نوع بار</td>
                            <td>ساعت اتمام</td>
                            <td>ساعت شروع</td> 
                            <td>تاریخ حمل</td>
                            <td>شرکت حمل و نقل</td> 
                            <td>مقصد بار</td> 
                            <td>مبدا بار</td> 
                            <td>رهگیری</td> 
                            <td>وضعیت حمل</td> 
                            <td>ردیف</td> 
                        </tr>
                        </thead>
                        <tbody>
                        <!-- show carry fields -->
                        @foreach($loads as $load)

                            <!-- check_status_color -->
                            @if($load['value_mohasebe'] <= 0 )
                                @php
                                    $color="#fd4646";
                                @endphp
                            @elseif($load['te'] < $timeNow)
                                @php 
                                    $color="#ff7676"
                                @endphp
                            @else
                                @php
                                    $color="#e5e576" 
                                @endphp
                            @endif

                            <tr style="background-color: {{$color}};">
                                <td>{{$load['TypeLoad']}}</td>
                                <td>{{$load['TypeTakh']}}</td>
                                <td>{{$load['TypeVasile']}}</td>
                                <td>{{$load['price']}}</td>

                                <!-- check_payment -->
                                @if($load['id_pardakht'] == 0)
                                <td>هرتن</td>
                                @else
                                <td>هر سرویس</td>
                                @endif

                                <!-- check_type -->
                                @if($load['id_kerayeh'] == 0)
                                <td>خالص</td>
                                @else
                                <td>ناخالص</td>
                                @endif

                                <!-- check_service -->
                                @if($load['id_mohasebe'] == 0)
                                <td>تن</td>
                                @else
                                <td>ناوگان</td>
                                @endif

                                <td>{{$load['value_mohasebe']}}</td>
                                <td>{{$load['packagekala']}}</td>
                                <td>{{$load['time_end']}}</td>
                                <td>{{$load['time_start']}}</td>
                                <td>{{$load['datebar']}}</td>
                                <td>{{$load['ellam']}}</td>
                                <td>{{$load['destinition']}}</td>
                                <td>{{$load['source']}}</td>
                                <td>{{$load['rahgiri']}}</td>

                                <!-- check_status -->
                                @if($load['value_mohasebe'] <= 0 )
                                    <td>اتمام مقدار بار</td>
                                @elseif($load['te'] < $timeNow)
                                    <td>اتمام زمان بارگیری</td>
                                @else
                                    <td>درانتظار حمل</td>
                                @endif

                                <td>{{$loop->iteration}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection