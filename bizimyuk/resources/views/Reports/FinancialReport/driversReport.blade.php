@extends('Layout.master')

@section('content')
<!-- hidden scroll bar -->
<style>
    body {overflow: hidden}
</style>

<section class="mini" id="work-process">
    <div class="mini-content">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <!-- title -->
                   <div class="info">
                        <h1> گزارش مالی  </h1>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-9 col-lg-9 col-md-9 col-sm-9 ">
                    <div class="chart">

                        <!-- get overal info -->
                        <div class="overal-info">
                            @isset($overalWeekNow)
                                <span class="overal-titr">میزان هفته جاری : <span class="overal-value">{{number_format($overalWeekNow)}}</span></span>
                                <span class="overal-titr">میزان هفته گذشته : <span class="overal-value">{{number_format($overalWeekLast)}}</span></span>
                                <span class="overal-titr">تعداد بار رفته در هفته جاری : <span class="overal-value">{{number_format($carryInWeek)}}</span></span>
                            @endisset

                            @isset( $overalMonthNow)
                                <span class="overal-titr">میزان ماه جاری : <span class="overal-value">{{number_format($overalMonthNow)}}</span></span>
                                <span class="overal-titr">میزان ماه گذشته : <span class="overal-value">{{number_format($overalMonthLast)}}</span></span>
                                <span class="overal-titr">تعداد بار رفته در ماه جاری : <span class="overal-value">{{number_format($carryInMonth)}}</span></span>
                            @endisset
                        </div>

                        <!-- chart -->
                        <canvas id="myChart" style="width:100%;max-width:600px"></canvas>

                        <!-- get overal info taraz -->
                        <div  class="overal-taraz">
                            @isset( $overalMonthNow)
                            <span class="overal-value">{{number_format($overalMonthNow - $overalMonthLast)}}</span><span class="overal-titr">: تراز مالی </span>
                            @endisset
                            @isset($overalWeekNow)
                            <span class="overal-value">{{number_format($overalWeekNow - $overalWeekLast)}}</span><span class="overal-titr">: تراز مالی  </span>
                            @endisset
                        </div>
                    </div>
                </div>

                <div class="col-3 col-lg-3 col-md-3 col-sm-3 col-3">
                    <!-- filter-chart -->
                    <div class="chart-filter">
                        <ul class="list-links">
                            <a href="{{route('financialWeekReport')}}"><li> هفته جاری </li></a>
                            <a href="{{route('financialMonthReport')}}" ><li>ماه جاری </li></a>
                            <a class="report-modal" data-toggle="modal" data-target="#RangeOFReport"><li>انتخاب بازه تاریخ</li></a>
                        </ul>
                        <br>
                        <!-- diffrence in last and now amounts -->
                        @isset($amountsLast)
                         <button type="button"  class="btn btn-success btn-diffrent"  onclick="updateChartData({{Js::from($amounts)}} , {{Js::from($amountsLast)}})">مقایسه مالی</button>
                        @endisset
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>


<!-- chart-script -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

@push('script-link')
    <!-- reposrt-script -->
    <script src="{{asset('js/Reports/FinancialReport/report_driver.js')}}"></script>
@endpush

<script>
    // send data to chart.js
    var amounts={!!Js::from($amounts)!!};
    var period={!!Js::from($period)!!};
</script>


<!-- modal -->
<!-- filter chart with range of date -->
<div class="modal fade" id="RangeOFReport" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">انتخاب بازه تاریخ گزارش </h5>
      </div>
      <!-- send data with form  for range-->
      <form action="{{route('financialRangeReport')}}" method="post">
        @csrf
      <div class="modal-body">
            <label for="startDate">: تاریخ شروع</label>
            <input type="text" name="startDate"  class="form-control date-input">
            <label for="endDate">: تاریخ پایان</label>
            <input type="text" name="endDate"  class="form-control date-input">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">لغو</button>
        <button type="submit" class="btn btn-success">نمایش</button>
      </div>
      </form>
    </div>
  </div>
</div>

@endsection
