@extends('Layout.master')

@section('content')

@push('drivers-filter')
<!-- search auto -->
<input type="text" id="search" class="searchDriver" placeholder="جستجو...." />

<!-- filter table in menu-->
<li><a class="dropdown-style"><div class="dropdown">
  <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
  وضعیت رانندگان
  </button>
  <div class="dropdown-menu">
    <form action="{{route('filter-driver' , '1')}}" method="get">@csrf<button class="dropdown-item" type="submit">فعال</button></form>
    <form action="{{route('filter-driver' , '0')}}" method="get">@csrf<button class="dropdown-item" type="submit">غیرفعال</button></form>
    <form action="{{route('filter-driver' , 'all')}}" method="get">@csrf<button class="dropdown-item" type="submit">همه موارد</button></form>
  </div>
</div></a></li>

@endpush

<section class="mini" id="work-process">
    <div class="mini-content">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <!-- title -->
                   <div class="info">
                        <h1> رانندگان </h1>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="driver-table">

                    <!-- table -->
                    <table id="table">
                        <thead>
                        <tr>
                            <td></td>
                            <td>کیلومتر سفر</td>
                            <td>پلاک</td>
                            <td>تعداد بار رفته جاری</td>
                            <td>مکان بارگیری</td>
                            <td>اولویت</td>
                            <td>استان</td> 
                            <td>شماره تماس</td> 
                            <td> نام و نام خانوادگی </td>   
                            <td>وضعیت</td>
                        </tr>
                        </thead>
                        <!-- show company fields -->
                        <tbody>
                        @if($drivers)
                            @foreach($drivers as $driver)
                            <tr>
                                                        
                                <td><a href="{{route('show-edit-driver' , $driver->id )}}"><button class="btn btn-success">ویرایش</button></a></td>
                                <td></td>
                                <td>{{$driver->p}}</td>
                                <td>{{$driver->archive_count}}</td>
                                <td> {{$driver->description}}</td>
                                <td> {{$driver->olaviyat}}</td>
                                <td> {{$driver->ostan}}</td>
                                <td> {{$driver->phone}}</td>
                                <td> {{$driver->name ." ".$driver->family }} </td>

                                <!-- check-status -->
                                @if($driver->status == 1)
                                    <th class="status-success" ></th>
                                @else
                                    <th class="status-danger" ></th>
                                @endif

                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    <ul id="results"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Link-searchBox drivers -->
@push('script-link')
    <script src="{{asset('js/Drivers/driver_search_automaticly.js')}}"></script>
@endpush

@endsection