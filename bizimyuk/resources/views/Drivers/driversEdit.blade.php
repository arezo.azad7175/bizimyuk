@extends('Layout.master')

@push('drivers-filter')
<li><a href="{{route('drivers')}}">برگشت به رانندگان</a></li>
@endpush

@section('content')
<section class="mini" id="work-process">
    <div class="mini-content">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <!-- title -->
                   <div class="info">
                        <h1> ویرایش رانندگان </h1>
                    </div>
                </div>
            </div>
            <form action="{{route('edit-driver')}}" method="post">
                @csrf
                <!-- authentication for drivers -->
                <div class="row">
                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="box-edit">
                            <h3>احراز هویت</h3>
                            </br>
                            <div>
                            <!-- نام و نام خانوادگی  -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="name" >نام:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8">
                                    <input type="text" name="name" id="name" value="{{$driver->name}}" >
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="lastName">نام خانوادگی:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8">
                                    <input type="text" name="lastName" id="lastName" value="{{$driver->family}}">
                                </div>
                            </div>
                            <!-- َماره تماس -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="phone">شماره تماس:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8">
                                    <input type="text"  name="phone" id="phone" value="{{$driver->reg_phone}}"  readonly>
                                </div>
                            </div>
                            <!-- کارت ملی -->
                            <div class="row img-box">
                                <div class="col-12 col-lg-12 col-md-12 col-sm-12 ">
                                    <figure id="magnifying_area">
                                        <img src="{{asset('images/madarek/cardm.jpg')}}" id="magnifying_img">
                                    </figure>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="nationalCode">کد ملی:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <input type="text" oninput= "this.value = this.value.replace(/[^0-9]/g , '')" name="nationalCode" value="{{$driver->melicode}}" maxlength="10">
                                </div>
                            </div>

                            <!-- کارت هوشمند -->
                            <div class="row img-box">
                                <div class="col-12 col-lg-12 col-md-12 col-sm-12 ">
                                    <figure id="magnifying_area">
                                        <img src="{{asset('images/madarek/cardh.jpg')}}" id="magnifying_img">
                                    </figure>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="smartCardDriver">کارت هوشمند راننده:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8">
                                    <!-- input with validation -->
                                    <input type="text" oninput= "this.value = this.value.replace(/[^0-9]/g , '')" name="smartCardDriver"value="{{$driver->hoshmand}}" maxlength="8">
                                </div>
                            </div>

                            <!-- کارت هوشمند ناوگان -->
                            <div class="row img-box">
                                <div class="col-12 col-lg-12 col-md-12 col-sm-12 ">
                                    <figure id="magnifying_area">
                                        <img src="{{asset('images/madarek/cardv.jpg')}}" id="magnifying_img">
                                    </figure>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="smartCardNavgan">کارت هوشمند ناوگان:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8">
                                    <!-- input with validation -->
                                    <input type="text" oninput= "this.value = this.value.replace(/[^0-9]/g , '')" name="smartCardNavgan" value="{{$driver->hoshmandv}}" maxlength="8">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>


                <!-- navgan part -->
                <div class="row">
                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="box-edit">
                            <h3>مشخصات ناوگان </h3>
                            </br>
                            <div>
                            <!-- اطلاعات ناوگان -->
                            <!-- نوع ناوگان -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="type_nav" >نوع ناوگان:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <input type="text" name="type_nav" value="{{$driver->type_nav}}">
                                </div>
                            </div>
                            <!-- نام برند -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="brand_name" >نام برند:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <input type="text" name="brand_name" value="{{$driver->brand_name}}">
                                </div>
                            </div>
                            <!-- نوع وسیله نقلیه -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="vasile" > نوع وسیله نقلیه:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <select name="vasile">
                                        <!-- rules for selected -->
                                        @for ($i=0; $i < count($type_car); $i++) 
                                            <option value="{{$type_car[$i]->id}}" {{($driver->id_vasile == $type_car[$i]->id) ? "selected" :""}}>{{$type_car[$i]->name}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <!-- سال ساخت -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="create_year">سال ساخت:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <!-- input with validation -->
                                    <input type="text" oninput= "this.value = this.value.replace(/[^0-9]/g , '')" name="create_year"  value="{{$driver->create_year}}" maxlength="4"  style="font-family: sans-serif;">
                                </div>
                            </div>
                            <!-- نوع تخلیه بار -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="unloading_type">نوع تخلیه بار:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <select name="unloading_type" >
                                        <!-- rules for selected -->
                                        @for ($i=0; $i < count($type_load); $i++) 
                                            <option value="{{$type_load[$i]->id}}" {{($driver->id_load == $type_load[$i]->id) ? "selected" :""}}>{{$type_load[$i]->name}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <!-- نوع اتاق -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="type_room" >نوع اتاق:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <select name="type_room" >
                                        <!-- rules for selected -->
                                        @for ($i=0; $i < count($type_room); $i++) 
                                            <option value="{{$type_room[$i]->id}}" {{($driver->id_tkh == $type_room[$i]->id) ? "selected" :""}}>{{$type_room[$i]->name}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div> 
                            <!-- درب اتاق عقب -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="Back_room_door">درب اتاق عقب:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <select name="Back_room_door" >
                                        <!-- rule for selected -->
                                        @for ($i=0; $i < count($type_oback); $i++) 
                                            <option value="{{$type_oback[$i]->id}}" {{($driver->id_oback == $type_oback[$i]->id) ? "selected" :""}}>{{$type_oback[$i]->name}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <br>
                            <!-- پلاک -->
                            <div class="row">
                                <div class="col-12 col-lg-4 col-md-4 col-sm-12">
                                    <label>پلاک:</label>
                                </div>
                                <div class="col-12 col-lg-8 col-md-8 col-sm-12">
                                    <div class="plate">
                                    <div class="license-plate">
                                        <div class="blue-column">
                                            <div class="flag">
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                            </div>
                                            <div class="text">
                                                <div>I.R.</div>
                                                <div>IRAN</div>
                                            </div>
                                        </div>
                                        <input type="text" oninput= "this.value = this.value.replace(/[^0-9]/g , '')" maxlength="3" name ="p3" placeholder="111" value="{{$driver->p3}}">
                                        <span class="alphabet-column">
                                        ع
                                        </span>
                                        <input type="text" oninput= "this.value = this.value.replace(/[^0-9]/g , '')"  maxlength="2" name ="p1" placeholder="11" class="p1" value="{{$driver->p1}}">
                                        <div class="iran-column">
                                            <input type="text" class="iran"  value="ایران" disabled>
                                            <input type="text" oninput= "this.value = this.value.replace(/[^0-9]/g , '')"  maxlength="2" name ="p4" value="{{$driver->p4}}">
                                        </div>
                                    </div>
                                </div>
                                </div>

                            </div>
                            <br>
                            <!-- بیمه شخص ثالت -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="bime_saleth">انقضا بیمه شخص ثالث:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <!-- get dte with verta format -->
                                    <input type="text" name="bime_saleth" value="{{Verta::parse($driver->dates)->datetime()->format('Y/n/j')}}" class="date-input">
                                </div>
                            </div>
                            <div class="row img-box">
                                <div class="col-12 col-lg-12 col-md-12 col-sm-12 ">
                                    <figure id="magnifying_area">
                                        <img src="{{asset('images/madarek/img_sales.jpg')}}" alt="">
                                    </figure>
                                </div>
                            </div>
                     
                            <!-- بیمه سلامت -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="bime_health">انقضا بیمه سلامت:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <!-- get date with verta format -->
                                    <input type="text" name="bime_health" value="{{Verta::parse($driver->datebs)->datetime()->format('Y/n/j')}}" class="date-input">
                                </div>
                            </div>
                            <div class="row img-box">
                                <div class="col-12 col-lg-12 col-md-12 col-sm-12 ">
                                    <figure id="magnifying_area">
                                        <img src="{{asset('images/madarek/img_salamat.jpg')}}" alt="">
                                    </figure>
                                </div>
                            </div>

                            <!-- معایته فنی -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="fani">انقضا معاینه فنی:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <!-- get date with verta format -->
                                    <input type="text" name="fani" value="{{ Verta::parse($driver->dateb)->datetime()->format('Y/n/j')}}" class="date-input">
                                </div>
                            </div>
                            <div class="row img-box">
                                <div class="col-12 col-lg-12 col-md-12 col-sm-12 ">
                                    <figure id="magnifying_area">
                                        <img src="{{asset('images/madarek/img_fani.jpg')}}" alt="">
                                    </figure>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

                <!-- License part -->
                <div class="row">
                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="box-edit">
                            <div>
                                <!-- وضعیت -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="status">وضعیت:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <select name="status" id="status" >
                                        <!-- rules for selected -->
                                        <option value="0" @if($driver->status == 0) selected @endif>غیرفعال</option>
                                        <option value="1" @if($driver->status == 1) selected @endif>فعال</option>
                                    </select>
                                </div>
                            </div>
                            <!-- علت غیرفعالی -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="reason">علت غیرفعالی:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <!-- rules for active or not active -->
                                    <textarea name="reason" id="reason" cols="30" rows="10" placeholder="علت غیرفعالی را وارد کنید" @if($driver->status == 1) disabled @endif></textarea>
                                </div>
                            </div>
                            <!-- اولویت راننده -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="olaviat">اولویت راننده:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <select name="olaviat" >
                                        <!-- rules for show and selected -->
                                        @for ($i=0; $i <= 50; $i++) 
                                            <option value="{{$i}}" {{($driver->olaviyat == $i) ? "selected" :""}}>{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <!-- مکان مجاز بارگیری  -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="loading_place">مکان مجاز بارگیری:</label>
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <select name="loading_place" >
                                        <!-- rules for show and selected -->
                                        @for ($i=0; $i < count($loading_place); $i++) 
                                            <option value="{{$loading_place[$i]->place}}" {{($driver->loading_place == $loading_place[$i]->place) ? "selected" :""}}>{{$loading_place[$i]->name}}</option>
                                        @endfor
                                        <option value="" {{($driver->loading_place == "") ? "selected" :""}}>انتخاب نشده است</option>
                                    </select>

                                </div>
                            </div>
                            <!-- قرارداد -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <label for="agreement">قرارداد:</label>
                                </div>

                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <select name="agreement" >
                                        <!-- rules for show and selected  -->
                                        @for ($i=0; $i <= 20; $i++)     
                                            <option value="{{$i}}" {{($driver->gharardad == $i) ? "selected" :""}} >{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <br>
                            <br>
                            <!-- submit and update info -->
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                </div>
                                <div class="col-8 col-lg-8 col-md-8 col-sm-8 ">
                                    <button type="submit" class="btn btn-success">ثبت و به روز رسانی اطلاعات</button>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>


<!-- after updated show modal-->
@if(Session::has('success_update'))
<script>
// show modal for send sms
$(document).ready(function()
{
    $('#driverModal').modal('show');
});
</script>
@endif


<!-- modal -->
<!-- driver-sms -->
<div class="modal fade" id="driverModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="driverModalLongTitle">ویرایش اطلاعات</h5>
      </div>
      <div class="modal-body">
        <!-- show_driver_name -->
        <p> اطلاعات آقای {{$driver->family}} با موفقیت به روزرسانی شد </p>
        <p> آیا میخواهید به ایشان پیام ارسال شود؟ </p>
      </div>
      <!-- send_data_with_ajax -->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"> خیر </button>
        <button type="button" class="btn btn-success" onclick="driverSendSms('{{Session::get('reason_status')}}')"> بلی </button>
      </div>
    </div>
  </div>
</div>

@endsection

