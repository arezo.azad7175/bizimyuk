-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: bizimyukdb:3306
-- Generation Time: Apr 25, 2024 at 05:26 PM
-- Server version: 8.0.30
-- PHP Version: 8.2.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `charming_engelbart`
--

-- --------------------------------------------------------

--
-- Table structure for table `archive`
--

CREATE TABLE `archive` (
  `id` bigint UNSIGNED NOT NULL,
  `ds` int NOT NULL,
  `status` int DEFAULT NULL,
  `price` int NOT NULL,
  `closing` int NOT NULL,
  `reset` int NOT NULL,
  `tc` int NOT NULL,
  `time_control` time NOT NULL,
  `nobat` int DEFAULT NULL,
  `time_step2` time NOT NULL,
  `date_step2` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_rahgiri` bigint DEFAULT NULL,
  `rahgiri` bigint DEFAULT NULL,
  `id_bar` int NOT NULL,
  `reg_phone` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `archive`
--

INSERT INTO `archive` (`id`, `ds`, `status`, `price`, `closing`, `reset`, `tc`, `time_control`, `nobat`, `time_step2`, `date_step2`, `main_rahgiri`, `rahgiri`, `id_bar`, `reg_phone`) VALUES
(1, 14030125, 3, 58000, 0, 0, 512, '05:12:00', 1, '05:12:00', '1403/01/29', 14020116240002001, 14020116240002001, 1, 9199603761),
(2, 14030126, 100, 58000, 0, 0, 456, '04:56:00', NULL, '04:56:00', '1403/01/29', 14020116240002002, 14020116240002002, 2, 9199603762),
(3, 14030127, 2, 58000, 0, 0, 512, '05:12:00', 1, '05:12:00', '1403/01/29', 14020116240002003, 14020116240002003, 3, 9199603763),
(4, 14030128, 100, 58000, 0, 0, 216, '02:16:00', NULL, '02:16:00', '1403/01/29', 14020116240002004, 14020116240002004, 4, 9199603764),
(5, 14030129, 5, 58000, 1, 1, 304, '03:04:00', NULL, '03:04:00', '1403/01/29', 14020116240002005, 14020116240002005, 5, 9199603765),
(25, 14030119, 3, 58000, 0, 0, 513, '05:13:00', 3, '05:13:00', '1403/01/29', 14020116240002001, 14020116240002001, 1, 9199603761),
(26, 14030119, 3, 58000, 0, 0, 513, '05:13:00', 2, '05:13:00', '1403/01/29', 14020116240002001, 14020116240002001, 1, 9199603761),
(27, 14030120, 4, 58000, 0, 0, 513, '05:13:00', 2, '05:13:00', '1403/01/29', 14020116240002002, 14020116240002002, 2, 9199603762),
(28, 14030120, 4, 58000, 0, 0, 513, '05:13:00', 1, '05:13:00', '1403/01/29', 14020116240002002, 14020116240002002, 2, 9199603762),
(29, 14030107, 2, 58000, 0, 0, 513, '05:13:00', 2, '05:13:00', '1403/01/29', 14020116240002003, 14020116240002003, 3, 9199603763),
(30, 14030107, 4, 58000, 0, 0, 513, '05:13:00', 3, '05:13:00', '1403/01/29', 14020116240002003, 14020116240002003, 3, 9199603763),
(31, 14030116, 3, 58000, 0, 0, 512, '05:12:00', 1, '05:12:00', '1403/01/29', 14020116240002004, 14020116240002004, 4, 9199603764),
(32, 14030116, 5, 58000, 1, 1, 303, '03:03:00', NULL, '03:03:00', '1403/01/29', 14020116240002004, 14020116240002004, 4, 9199603764),
(33, 14030119, 100, 58000, 0, 0, 358, '03:58:00', NULL, '03:58:00', '1403/01/29', 14020116240002005, 14020116240002005, 5, 9199603765),
(34, 14030119, 4, 58000, 0, 0, 513, '05:13:00', 1, '05:13:00', '1403/01/29', 14020116240002005, 14020116240002005, 5, 9199603765);

-- --------------------------------------------------------

--
-- Table structure for table `bar`
--

CREATE TABLE `bar` (
  `id` bigint UNSIGNED NOT NULL,
  `mainbar_index` int NOT NULL,
  `id_vasile` int NOT NULL,
  `id_tkh` int NOT NULL,
  `id_load` int NOT NULL,
  `id_oback` int NOT NULL,
  `ddate` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ttime` time NOT NULL,
  `reg_phone` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bar`
--

INSERT INTO `bar` (`id`, `mainbar_index`, `id_vasile`, `id_tkh`, `id_load`, `id_oback`, `ddate`, `ttime`, `reg_phone`) VALUES
(1, 1, 3, 2, 1, 3, '14030116', '15:30:00', 9199603761),
(2, 2, 4, 3, 2, 4, '14030116', '15:30:00', 9199603762),
(3, 3, 6, 5, 1, 1, '14030116', '15:30:00', 9199603763),
(4, 4, 2, 4, 2, 4, '14030116', '15:30:00', 9199603764),
(5, 5, 1, 1, 1, 4, '14030116', '15:30:00', 9199603765);

-- --------------------------------------------------------

--
-- Table structure for table `b_archive`
--

CREATE TABLE `b_archive` (
  `id` bigint UNSIGNED NOT NULL,
  `ds` int NOT NULL,
  `status` int NOT NULL,
  `price` int NOT NULL,
  `closing` int NOT NULL,
  `reset` int NOT NULL,
  `te` int NOT NULL,
  `timecontrol` time NOT NULL,
  `nobat` int NOT NULL,
  `time_step_2` time NOT NULL,
  `date_step_2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rahgiri` bigint NOT NULL,
  `main_rahgiri` bigint NOT NULL,
  `id_bar` int NOT NULL,
  `reg_phone` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `b_archive`
--

INSERT INTO `b_archive` (`id`, `ds`, `status`, `price`, `closing`, `reset`, `te`, `timecontrol`, `nobat`, `time_step_2`, `date_step_2`, `rahgiri`, `main_rahgiri`, `id_bar`, `reg_phone`) VALUES
(1, 13890114, 4, 85000, 0, 0, 750, '07:50:00', 60, '10:30:00', '1389/01/14', 1389011424001001, 1389011424001001060, 2, 9199603763);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint UNSIGNED NOT NULL,
  `company_id` int NOT NULL,
  `company_name` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `company_id`, `company_name`) VALUES
(1, 1, 'دستاوردان نوین'),
(2, 2, 'پویاگران برتر'),
(3, 3, 'کاوشگران نوین');

-- --------------------------------------------------------

--
-- Table structure for table `ehraz`
--

CREATE TABLE `ehraz` (
  `id` bigint UNSIGNED NOT NULL,
  `reg_phone` bigint NOT NULL,
  `melicode` bigint NOT NULL,
  `hoshmand` bigint NOT NULL,
  `hoshmandv` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ehraz`
--

INSERT INTO `ehraz` (`id`, `reg_phone`, `melicode`, `hoshmand`, `hoshmandv`) VALUES
(1, 9191453125, 4900458532, 1234567, 8529634),
(2, 9199603763, 4900564675, 1478523, 6549871),
(3, 9199603765, 4900564675, 1478523, 6549871),
(4, 9199603769, 4900564675, 1478523, 6549871),
(5, 9199603766, 4900564675, 1478523, 6549871),
(6, 9191433541, 4900564675, 1478523, 6549871);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `havale`
--

CREATE TABLE `havale` (
  `id` bigint UNSIGNED NOT NULL,
  `rahgiri` bigint NOT NULL,
  `nobat` int DEFAULT NULL,
  `ddate` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `dd` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ttime` time NOT NULL,
  `source` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `packagekala` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL,
  `gharardad` int NOT NULL,
  `tonaz` int NOT NULL,
  `barname` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `value_ts` int NOT NULL,
  `phone` bigint NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pelak` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_archive` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `havale`
--

INSERT INTO `havale` (`id`, `rahgiri`, `nobat`, `ddate`, `dd`, `ttime`, `source`, `target`, `company`, `packagekala`, `status`, `gharardad`, `tonaz`, `barname`, `value_ts`, `phone`, `name`, `pelak`, `id_archive`) VALUES
(1, 14020116240002001, 1, '1403/01/16', '14030116', '18:20:00', 'زنجان', 'زنجان-دندی', 'پویاگران برتر', 'کیسه جانبو', 3, 1, 25, '1254697', 20, 9199603761, 'تقیلو', '123 12 87 ع', 1),
(7, 14020116240002003, 1, '1403/01/16', '14030116', '18:20:00', 'زنجان', 'زنجان-دندی', 'پویاگران برتر', 'کیسه جانبو', 2, 1, 40, '1254697', 20, 9199603763, 'خزلی', '123 12 87 ع', 3),
(8, 14020116240002004, 1, '1403/01/19', '14030119', '18:20:00', 'زنجان', 'زنجان-دندی', 'پویاگران برتر', 'کیسه جانبو', 3, 1, 50, '1254697', 20, 9199603764, 'کلامی', '123 12 87 ع', 31),
(11, 14020116240002005, NULL, '1403/01/25', '14030125', '10:15:00', 'زنجان', 'زنجان-دندی', 'کیمیاگران برتر', 'بار گمرک', 100, 1, 50, '123654789', 20, 9199603765, 'حسنی', '125 12 87 ع', 33),
(14, 14020116240002002, NULL, '1403/01/20', '14030120', '10:15:00', 'زنجان', 'زنجان-دندی', 'کیمیاگران برتر', 'بار گمرک', 100, 1, 30, '123654789', 20, 9199603762, 'حسام صفری', '125 12 87 ع', 2),
(22, 14020116240002001, 3, '1403/01/16', '14030116', '18:20:00', 'زنجان', 'زنجان-دندی', 'پویاگران برتر', 'کیسه جانبو', 3, 1, 25, '1254697', 20, 9199603761, 'زمانی', '123 12 87 ع', 25),
(23, 14020116240002001, 2, '1403/01/16', '14030116', '18:20:00', 'زنجان', 'زنجان-دندی', 'پویاگران برتر', 'کیسه جانبو', 3, 1, 25, '1254697', 20, 9199603761, 'جعفری', '123 12 87 ع', 26),
(24, 14020116240002002, 2, '1403/01/20', '14030120', '10:15:00', 'زنجان', 'زنجان-دندی', 'کیمیاگران برتر', 'بار گمرک', 4, 1, 30, '123654789', 20, 9199603762, 'صفری', '125 12 87 ع', 27),
(25, 14020116240002002, 1, '1403/01/20', '14030120', '10:15:00', 'زنجان', 'زنجان-دندی', 'کیمیاگران برتر', 'بار گمرک', 4, 1, 30, '123654789', 20, 9199603762, 'جلیل', '125 12 87 ع', 28),
(26, 14020116240002003, 2, '1403/01/16', '14030116', '18:20:00', 'زنجان', 'زنجان-دندی', 'پویاگران برتر', 'کیسه جانبو', 2, 1, 40, '1254697', 20, 9199603763, 'شیما', '123 12 87 ع', 29),
(27, 14020116240002003, 3, '1403/01/16', '14030116', '18:20:00', 'زنجان', 'زنجان-دندی', 'پویاگران برتر', 'کیسه جانبو', 4, 1, 30, '1254697', 20, 9199603763, 'ثریا', '123 12 87 ع', 30),
(28, 14020116240002004, NULL, '1403/01/19', '14030119', '18:20:00', 'زنجان', 'زنجان-دندی', 'پویاگران برتر', 'کیسه جانبو', 5, 1, 40, '1254697', 20, 9199603764, 'سفری', '123 12 87 ع', 32),
(29, 14020116240002004, NULL, '1403/01/19', '14030119', '18:20:00', 'زنجان', 'زنجان-دندی', 'پویاگران برتر', 'کیسه جانبو', 100, 1, 40, '1254697', 20, 9199603764, 'جفنگی', '123 12 87 ع', 4),
(30, 14020116240002005, 1, '1403/01/25', '14030125', '10:15:00', 'زنجان', 'زنجان-دندی', 'کیمیاگران برتر', 'بار گمرک', 4, 1, 50, '123654789', 20, 9199603765, 'قربانی', '125 12 87 ع', 34),
(31, 14020116240002005, NULL, '1403/01/25', '14030125', '10:15:00', 'زنجان', 'زنجان-دندی', 'کیمیاگران برتر', 'بار گمرک', 5, 1, 50, '123654789', 20, 9199603765, 'ملایی', '125 12 87 ع', 5);

-- --------------------------------------------------------

--
-- Table structure for table `loading_place`
--

CREATE TABLE `loading_place` (
  `id` bigint UNSIGNED NOT NULL,
  `place` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `loading_place`
--

INSERT INTO `loading_place` (`id`, `place`, `description`) VALUES
(1, 'A', 'انگوران'),
(2, 'B', 'گمرک'),
(3, 'C', 'ابهر');

-- --------------------------------------------------------

--
-- Table structure for table `mainbar`
--

CREATE TABLE `mainbar` (
  `id` bigint UNSIGNED NOT NULL,
  `rahgiri` bigint NOT NULL,
  `value_mohasebe` int NOT NULL,
  `datebar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `destinition` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ellam` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_start` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_end` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `te` int NOT NULL,
  `packagekala` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_mohasebe` int NOT NULL,
  `id_kerayeh` int NOT NULL,
  `id_pardakht` int NOT NULL,
  `price` int NOT NULL,
  `reg_phone` bigint NOT NULL,
  `value_aval` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mainbar`
--

INSERT INTO `mainbar` (`id`, `rahgiri`, `value_mohasebe`, `datebar`, `source`, `destinition`, `ellam`, `time_start`, `time_end`, `te`, `packagekala`, `id_mohasebe`, `id_kerayeh`, `id_pardakht`, `price`, `reg_phone`, `value_aval`) VALUES
(1, 14020116240002001, 17, '14030116', 'زنجان-دندی', 'زنجان-زنجان', 'دندی بار', '9:30', '12:50', 1250, 'کیسه جانبو', 1, 1, 1, 36000, 9199603761, 20),
(2, 14020116240002002, 18, '14030116', 'زنجان', 'زنجان-دندی', 'دندی بار', '14:10', '18:30', 1830, 'کیسه جانبو', 0, 1, 1, 85000, 9199603762, 20),
(3, 14020116240002003, 17, '14030116', 'زنجان-دندی', 'زنجان-زنجان', 'دندی بار', '9:30', '12:50', 1250, 'کیسه جانبو', 1, 1, 1, 36000, 9199603763, 20),
(4, 14020116240002004, 19, '14030116', 'زنجان', 'زنجان-دندی', 'دندی بار', '14:10', '18:30', 1830, 'کیسه جانبو', 0, 1, 1, 85000, 9199603764, 20),
(5, 14020116240002005, 19, '14030116', 'زنجان', 'زنجان-دندی', 'دندی بار', '14:10', '18:30', 1830, 'کیسه جانبو', 0, 1, 1, 85000, 9199603765, 20);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2024_04_03_121058_create_havale_table', 1),
(6, '2024_04_04_141955_create_profile_table', 1),
(7, '2024_04_04_142427_create_typetkh_table', 1),
(8, '2024_04_04_142513_create_loading_place_table', 1),
(9, '2024_04_04_142943_create_typecar_table', 1),
(10, '2024_04_04_143050_create_typeoback_table', 1),
(11, '2024_04_04_143203_create_ehraz_table', 1),
(12, '2024_04_04_143411_create_typeload_table', 1),
(13, '2024_04_04_143458_create_navgan_table', 1),
(14, '2024_04_04_144213_create_archive_table', 1),
(15, '2024_04_04_144646_create_companies_table', 1),
(16, '2024_04_04_144802_create_b_archive_table', 1),
(17, '2024_04_04_144904_create_mainbar_table', 1),
(18, '2024_04_04_145504_create_bar_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `navgan`
--

CREATE TABLE `navgan` (
  `id` bigint UNSIGNED NOT NULL,
  `reg_phone` bigint NOT NULL,
  `id_vasile` int NOT NULL,
  `id_load` int NOT NULL,
  `id_oback` int NOT NULL,
  `id_tkh` int NOT NULL,
  `p` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `p1` int NOT NULL,
  `p2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `p3` int NOT NULL,
  `p4` int NOT NULL,
  `type_nav` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_year` int NOT NULL,
  `dates` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ds` int NOT NULL,
  `dateb` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `db` int NOT NULL,
  `datebs` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `dbs` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `navgan`
--

INSERT INTO `navgan` (`id`, `reg_phone`, `id_vasile`, `id_load`, `id_oback`, `id_tkh`, `p`, `p1`, `p2`, `p3`, `p4`, `type_nav`, `brand_name`, `create_year`, `dates`, `ds`, `dateb`, `db`, `datebs`, `dbs`) VALUES
(1, 9199603761, 5, 1, 2, 4, '123 12 87 ع', 12, 'ع', 123, 87, 'volvo', 'fm9', 1390, '1403/02/05', 14030320, '1403/03/15', 14030315, '1403/03/20', 14030320),
(2, 9199603762, 3, 1, 2, 4, '123 12 87 ع', 12, 'ع', 123, 87, 'volvo', 'fm9', 1390, '1403/02/05', 14030320, '1403/03/15', 14030315, '1403/03/20', 14030320),
(3, 9199603763, 7, 1, 2, 4, '123 12 87 ع', 12, 'ع', 123, 87, 'volvo', 'fm9', 1390, '2645/07/15', 26450821, '2645/08/26', 26450826, '2645/08/21', 26450821),
(4, 9199603764, 6, 3, 2, 4, '123 12 87 ع', 12, 'ع', 123, 87, 'volvo', 'fm9', 1390, '1403/02/05', 14030205, '1403/03/15', 14030315, '1403/03/20', 14030320),
(5, 9199603765, 5, 3, 2, 4, '123 12 87 ع', 12, 'ع', 123, 87, 'volvo', 'fm9', 1390, '1403/02/05', 14030205, '1403/03/15', 14030315, '1403/03/20', 14030320),
(6, 9199603766, 4, 1, 2, 4, '123 12 87 ع', 12, 'ع', 123, 87, 'volvo', 'fm9', 1390, '1403/02/05', 14030305, '1403/03/15', 14030315, '1403/03/20', 14030320),
(7, 9199503767, 1, 3, 2, 4, '123 12 87 ع', 12, 'ع', 123, 87, 'volvo', 'fm9', 1390, '1403/02/05', 14030205, '1403/03/15', 14030315, '1403/03/20', 14030320),
(8, 9199603768, 5, 3, 2, 4, '123 12 87 ع', 12, 'ع', 123, 87, 'volvo', 'fm9', 1390, '1403/02/05', 14030205, '1403/03/15', 14030315, '1403/03/20', 14030320),
(9, 9199603775, 4, 3, 2, 4, '123 12 87 ع', 12, 'ع', 123, 87, 'volvo', 'fm9', 1390, '1403/02/05', 14030205, '1403/03/15', 14030315, '1403/03/20', 14030320),
(10, 9191433541, 4, 1, 2, 4, '123 12 87 ع', 12, 'ع', 123, 87, 'volvo', 'fm9', 1390, '1403/02/05', 14030320, '1403/03/15', 14030315, '1403/03/20', 14030320);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` bigint UNSIGNED NOT NULL,
  `id_mojaves` int NOT NULL,
  `allow` int NOT NULL,
  `reg_phone` bigint NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `family` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL,
  `olaviyat` int NOT NULL,
  `loading_place` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gharardad` int NOT NULL,
  `amount` int NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` bigint NOT NULL,
  `namemodir` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ostan` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `id_mojaves`, `allow`, `reg_phone`, `name`, `family`, `status`, `olaviyat`, `loading_place`, `gharardad`, `amount`, `address`, `phone`, `namemodir`, `ostan`) VALUES
(1, 1, 1, 9199603767, 'غلام', 'برزگر', 0, 1, 'C', 15, 10, 'گلشهر فاز 3 خیابان وصال 12 پلاک 5284', 9199603767, 'حسنی', 'زنجان'),
(2, 0, 0, 9199603766, 'کمال', 'زینلی', 0, 1, 'A', 15, 10, 'گلشهر فاز 3 خیابان وصال 12 پلاک 5284', 9199603766, 'حسنی', 'زنجان'),
(3, 0, 0, 9199603765, 'جلال', 'جلیلی', 1, 1, 'C', 15, 10, 'گلشهر فاز 3 خیابان وصال 12 پلاک 5284', 9199603765, 'حسنی', 'زنجان'),
(4, 1, 1, 9199603763, 'کریم', 'ترابی', 1, 1, 'B', 15, 10, ' شهرک کارمندان خیابان غزالی پلاک7', 9199603763, 'آرزو آزاد', 'زنجان'),
(5, 0, 0, 9199603763, 'کریم', 'ترابی', 1, 1, 'B', 15, 10, 'گلشهر فاز 3 خیابان وصال 12 پلاک 5284', 9199603763, 'حسنی', 'زنجان');

-- --------------------------------------------------------

--
-- Table structure for table `typecar`
--

CREATE TABLE `typecar` (
  `id` bigint UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `typecar`
--

INSERT INTO `typecar` (`id`, `name`) VALUES
(1, 'خاور و کامیونت'),
(2, 'وانت'),
(3, 'نیسان'),
(4, '911'),
(5, 'تک'),
(6, 'جفت'),
(7, 'تریلی');

-- --------------------------------------------------------

--
-- Table structure for table `typeload`
--

CREATE TABLE `typeload` (
  `id` bigint UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `typeload`
--

INSERT INTO `typeload` (`id`, `name`) VALUES
(1, 'کمپرسی'),
(2, 'ثابت');

-- --------------------------------------------------------

--
-- Table structure for table `typeoback`
--

CREATE TABLE `typeoback` (
  `id` bigint UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `typeoback`
--

INSERT INTO `typeoback` (`id`, `name`) VALUES
(1, 'درب عقب یک تکه'),
(2, 'درب عقب دو تکه'),
(3, 'درب عقب فاقد اهمیت'),
(4, 'بغل بازشو');

-- --------------------------------------------------------

--
-- Table structure for table `typetkh`
--

CREATE TABLE `typetkh` (
  `id` bigint UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `typetkh`
--

INSERT INTO `typetkh` (`id`, `name`) VALUES
(1, 'اتاق کارخونه'),
(2, 'کف میله'),
(3, 'یخچالی'),
(4, 'مسقف فلزی'),
(5, 'مسقف چادری');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `username` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `mobile`, `description`, `remember_token`, `updated_at`) VALUES
(1, 'arezoazad', '123456789', '09199603763', 'arezo azad', 'U6rrEeLSbWSohceIJDkZL9LdsZvXY7sGdmmPaUP4W8e91t86cI2ta5tqgNsv', '2024-04-11'),
(2, 'naser', '123456789', '09046482080', 'naser', 'TjftSpjrx8KrSuFb81CqvcEWKPYZkIdzUicO8adFwb7pRdsOhhNq2gJw05II', '2024-04-17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `archive`
--
ALTER TABLE `archive`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bar`
--
ALTER TABLE `bar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_archive`
--
ALTER TABLE `b_archive`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ehraz`
--
ALTER TABLE `ehraz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `havale`
--
ALTER TABLE `havale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loading_place`
--
ALTER TABLE `loading_place`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mainbar`
--
ALTER TABLE `mainbar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `navgan`
--
ALTER TABLE `navgan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typecar`
--
ALTER TABLE `typecar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typeload`
--
ALTER TABLE `typeload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typeoback`
--
ALTER TABLE `typeoback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typetkh`
--
ALTER TABLE `typetkh`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `archive`
--
ALTER TABLE `archive`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `bar`
--
ALTER TABLE `bar`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `b_archive`
--
ALTER TABLE `b_archive`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ehraz`
--
ALTER TABLE `ehraz`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `havale`
--
ALTER TABLE `havale`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `loading_place`
--
ALTER TABLE `loading_place`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mainbar`
--
ALTER TABLE `mainbar`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `navgan`
--
ALTER TABLE `navgan`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `typecar`
--
ALTER TABLE `typecar`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `typeload`
--
ALTER TABLE `typeload`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `typeoback`
--
ALTER TABLE `typeoback`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `typetkh`
--
ALTER TABLE `typetkh`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
